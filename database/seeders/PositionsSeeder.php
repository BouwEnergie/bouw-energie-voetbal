<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use Str;

class PositionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert([
            [
                'position' => 'aanvaller',
            ],
            [
                'position' => 'verdediger',
            ]
        ]);

    }
}
