<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('team_one_attack_player_one_id');
            $table->unsignedBigInteger('team_one_defend_player_two_id');
            $table->unsignedBigInteger('team_two_attack_player_three_id');
            $table->unsignedBigInteger('team_two_defend_player_four_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('team_one_attack_player_one_id')->references('id')->on('players');
            $table->foreign('team_one_defend_player_two_id')->references('id')->on('players');
            $table->foreign('team_two_attack_player_three_id')->references('id')->on('players');
            $table->foreign('team_two_defend_player_four_id')->references('id')->on('players');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
