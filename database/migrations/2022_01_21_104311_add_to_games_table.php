<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddToGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->after('team_two_defend_player_four_id', function ($table){
                $table->integer('team_one_total_score')->default(0);
                $table->integer('team_two_total_score')->default(0);
            });
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->dropColumn('team_one_total_score');
            $table->dropColumn('team_two_total_score');
        });
    }
}
