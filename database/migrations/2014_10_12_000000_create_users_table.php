<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
        DB::table('users')->insert([
            'name' => 'Meo Dröge',
            'email' => 'meo@egtw.nl',
            'password' => '$2y$10$A/dk0I3oKHuRAFiPFrxyqOPaK/GjmXUYwPKz.v.fFU0HCGYLIUcW6',
            'created_at' => '2021-11-03 15:24:10',
            'updated_at' => '2021-11-03 15:24:10',
        ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
