@extends('layouts.app')
@section('content')
    @if(isset($player))
        {{ Form::model($player, ['route' => ['player.update', $player->id], 'method' => 'put']) }}
        {{ Form::hidden('id', $player->id) }}
        @if (session ('error'))
            <div style="display: flex" class="alert alert-danger">
                <strong class="text-center">{{ session('error') }}</strong>
                <div style="display: flex; justify-content: right">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                </div>
            </div>
        @endif
    @else
        {{ Form::open(['route' => 'player.store', 'method' => 'post']) }}
        @if (session ('error'))
            <div style="display: flex" class="alert alert-danger">
                <strong class="text-center">{{ session('error') }}</strong>
                <div style="display: flex; justify-content: right">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                </div>
            </div>
        @endif
    @endif
            {{ Form::label('firstname', 'Voornaam')}}
            {{ Form::text('firstname', Request::old('firstname')) }}
            @error('firstname')
                <div class="alert alert-danger">{{$message}}</div>
            @enderror
            {{ Form::label('surname', 'Achternaam')}}
            {{ Form::text('surname', Request::old('surname')) }}
            @error('surname')
                <div class="alert alert-danger">{{$message}}</div>
            @enderror
            {{ Form::label('date_of_birth', 'Geboortedatum')}}
            {{ Form::date('date_of_birth', Request::old('date_of_birth')) }}
            @error('date_of_birth')
                <div class="alert alert-danger">{{$message}}</div>
            @enderror
            {{ Form::submit('Opslaan', ['class'=>'btn btn-primary', 'id' => 'save_button']) }}
        {{ Form::close() }}
@endsection