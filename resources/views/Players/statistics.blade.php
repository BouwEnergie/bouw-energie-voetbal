@extends('layouts.app')
@section('content')
<div class="container">
    <div class="h1 row justify-content-md-center">
        {{ $player->fullname }}
    </div>
    <div class="row justify-content-md-center">
        <div class="row justify-content-md-center">
            <div class="row justify-content-md-center">
                @isset($recentGame)
                <div class="h2">Recente wedstrijd</div>
                <div class="col; h6">{{ $recentGameData['recentGameDuration'] }}</div>
                <div class="row justify-content-md-center">
                    <div class="col; h5">
                        <div class="row">A: {{ $recentGame->teamOneAttackPlayerOneId->fullname ?? 'Niet gevonden'}}</div>
                        <div class="row">V: {{ $recentGame->teamOneDefendPlayerTwoId->fullname ?? 'Niet gevonden'}}</div>
                    </div>
                    <div class="col col-lg-2; h2">{{ $recentGameData['score'] }}</div>
                    <div class="col; h5">
                        <div class="row">A: {{ $recentGame->teamTwoAttackPlayerThreeId->fullname ?? 'Niet gevonden'}}</div>
                        <div class="row">V: {{ $recentGame->teamTwoDefendPlayerFourId->fullname ?? 'Niet gevonden'}}</div>
                    </div>
                    <div class="col; h6">{{ $recentGameData['recentGameDate'] }}  </div>
                </div>
                @endisset
            </div>
            <div class="col">
                <div class="h3 row justify-content-md-center">Totaal</div>
                <div class="card" style="width: 14rem;">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Wedstrijden: {{ $playerStatsTotal['playerGamesTotal'] ?? '0' }}</li>
                        <li class="list-group-item">Goals: {{ $playerStatsTotal['playerGoalsTotal'] ?? '0' }}</li>
                        <li class="list-group-item">Eigen goals: {{ $playerStatsTotal['playerOwnGoalsTotal'] ?? '0' }}</li>
                        <li class="list-group-item">Middenveld goals: {{ $playerStatsTotal['playerMidfieldGoalsTotal'] ?? '0' }}</li>
                        <li class="list-group-item">Gem goals per wedstrijd: {{ $playerStatsTotal['avgGoalsPerGameTotal'] }}</li>
                        <li class="list-group-item">Gem eigen goals per wedstrijd: {{ $playerStatsTotal['avgOwnGoalsPerGameTotal'] }}</li>
                        <li class="list-group-item">Gem middenveld goals per wedstrijd: {{ $playerStatsTotal['avgMidfieldGoalsPerGameTotal'] }}</li>
                        <li class="list-group-item">Win percentage: {{ $playerStatsTotal['winPercentageTotal'] ?? '0' }}%</li>
                        <li class="list-group-item">Komt voor: {{ $playerStatsTotal['gameAppearanceTotal'] ?? '0' }}%</li>
                    </ul>
                </div>
                <div class="h3 row justify-content-md-center">Aanval</div>
                <div class="card" style="width: 14rem;">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Wedstrijden: {{ $playerStatsAttack['playerGamesAttack'] ?? '0' }}</li>
                        <li class="list-group-item">Goals: {{ $playerStatsAttack['playerGoalsAttack'] ?? '0' }}</li>
                        <li class="list-group-item">Eigen goals: {{ $playerStatsAttack['playerOwnGoalsAttack'] ?? '0' }}</li>
                        <li class="list-group-item">Middenveld goals: {{ $playerStatsAttack['playerMidfieldGoalsAttack'] ?? '0' }}</li>
                        <li class="list-group-item">Gem goals per wedstrijd: {{ $playerStatsAttack['avgGoalsPerGameAttack'] }}</li>
                        <li class="list-group-item">Gem eigen goals per wedstrijd: {{ $playerStatsAttack['avgOwnGoalsPerGameAttack'] }}</li>
                        <li class="list-group-item">Gem middenveld goals per wedstrijd: {{ $playerStatsAttack['avgMidfieldGoalsPerGameAttack'] }}</li>
                        <li class="list-group-item">Win percentage: {{ $playerStatsAttack['winPercentageAttack'] ?? '0' }}%</li>
                        <li class="list-group-item">Komt voor: {{ $playerStatsAttack['gameAppearanceAttack'] ?? '0' }}%</li>
                    </ul>
                </div>
                <div class="h3 row justify-content-md-center">Verdediging</div>
                <div class="card" style="width: 14rem;">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Wedstrijden: {{ $playerStatsDefend['playerGamesDefend'] ?? '0' }}</li>
                        <li class="list-group-item">Goals: {{ $playerStatsDefend['playerGoalsDefend'] ?? '0' }}</li>
                        <li class="list-group-item">Eigen goals: {{ $playerStatsDefend['playerOwnGoalsDefend'] ?? '0' }}</li>
                        <li class="list-group-item">Gem goals per wedstrijd: {{ $playerStatsDefend['avgGoalsPerGameDefend'] ?? '0' }}</li>
                        <li class="list-group-item">Gem eigen goals per wedstrijd: {{ $playerStatsDefend['avgOwnGoalsPerGameDefend'] ?? '0' }}</li>
                        <li class="list-group-item">Win percentage: {{ $playerStatsDefend['winPercentageDefend'] ?? '0' }}%</li>
                        <li class="list-group-item">Komt voor: {{ $playerStatsDefend['gameAppearanceDefend'] ?? '0'}}%</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection