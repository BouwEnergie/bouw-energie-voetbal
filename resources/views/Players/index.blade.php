@extends('layouts.app')
@section('content')
    <div>
        @if (session('success'))
            <div style="display: flex" class="alert alert-success">
                <strong class="text-center">{{ session('success') }}</strong>
                <div style="display: flex; justify-content: right">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                </div>
            </div>
        @endif
        <div style="padding-left: 3.5rem; height: 70px">
            <a href="{{ route('player.create') }}" ; class="btn btn-success btn-lg shadow-sm">Nieuwe speler</a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col col-lg-2 font-weight-bold">Naam</div>
                <div class="col col-lg-2 font-weight-bold">Geboortedatum</div>
            </div>
            @foreach($players as $player)
                <div class="row">
                    <div class="col col-lg-2"> <a href="{{ route('player.show', $player->id) }}">{{ $player->fullname }}</a></div>
                    <div class="col col-lg-2">{{ $player->date_of_birth }}</div>
                    <div class="col">
                        <div class="row">
                            <a class="btn btn-outline-info" href="{{ route('player.displayStatistics', $player->id) }}">Info</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection