@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-md-center">
        <div class="h1">{{ $playerData['fullname'] }}</div>
    </div>
    <div class="row">
        <ul class="list-group">
            <li class="list-group-item">Geboortedatum: {{ $playerData['dateOfBirth'] }}</li>
            <li class="list-group-item">Aangemaakt: {{ $playerData['dateOfCreation'] }}</li>
        </ul>
    </div>
    <div class="row">
        <a class="btn btn-outline-secondary" href="{{ route('player.edit', $player->id) }}">Bewerken</a>
        {{ Form::open(['route'=> ['player.destroy', $player->id], 'onsubmit'=> "return confirm('Weet je het zeker?')", 'method'=>'POST']) }}
        {{ method_field('DELETE') }}
        {{ Form::submit('Verwijderen', ['class'=>'btn btn-outline-danger', 'id' => 'delete_button']) }}
        {{ Form::close() }}
    </div>
</div>
@endsection