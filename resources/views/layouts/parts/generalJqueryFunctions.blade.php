<!-- button delay functies -->

<script type="text/javascript">

    // opslaan buttons
    $('#save_button').click(function() {
        var button = $(this);
            button.prop('disabled', true);
            window.setTimeout(function() { 
                button.prop('disabled',false);
            } ,600);
            $('form').submit();
    });

    // game score buttons
    $('#player_one_attack_goals_plus_button').click(function() {
        var button = $(this);
        button.prop('disabled', true);
        window.setTimeout(function() { 
            button.prop('disabled',false);
        } ,200);
    });
    $('#player_one_attack_own_goals_plus_button').click(function() {
        var button = $(this);
        button.prop('disabled', true);
        window.setTimeout(function() { 
            button.prop('disabled',false);
        } ,200);
    });
    $('#player_one_midfield_goals_plus_button').click(function() {
        var button = $(this);
        button.prop('disabled', true);
        window.setTimeout(function() { 
            button.prop('disabled',false);
        } ,200);
    });
    $('#player_two_defend_goals_plus_button').click(function() {
        var button = $(this);
        button.prop('disabled', true);
        window.setTimeout(function() { 
            button.prop('disabled',false);
        } ,200);
    });
    $('#player_two_defend_own_goals_plus_button').click(function() {
        var button = $(this);
        button.prop('disabled', true);
        window.setTimeout(function() { 
            button.prop('disabled',false);
        } ,200);
    });
    $('#player_three_attack_goals_plus_button').click(function() {
        var button = $(this);
        button.prop('disabled', true);
        window.setTimeout(function() { 
            button.prop('disabled',false);
        } ,200);
    });
    $('#player_three_attack_own_goals_plus_button').click(function() {
        var button = $(this);
        button.prop('disabled', true);
        window.setTimeout(function() { 
            button.prop('disabled',false);
        } ,200);
    });
    $('#player_three_midfield_goals_plus_button').click(function() {
        var button = $(this);
        button.prop('disabled', true);
        window.setTimeout(function() { 
            button.prop('disabled',false);
        } ,200);
    });
    $('#player_four_defend_goals_plus_button').click(function() {
        var button = $(this);
        button.prop('disabled', true);
        window.setTimeout(function() { 
            button.prop('disabled',false);
        } ,200);
    });
    $('#player_four_defend_own_goals_plus_button').click(function() {
        var button = $(this);
        button.prop('disabled', true);
        window.setTimeout(function() { 
            button.prop('disabled',false);
        } ,200);
    });

</script>