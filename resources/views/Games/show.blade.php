@extends('layouts.app')
@section('content')
<div class="container">
    <div class="col">
        <div class="row justify-content-md-center">
            <div class="h1">{{ $game->id }}</div>
        </div>
        <div class="row justify-content-md-center">
            <a class="btn btn-warning" href="{{ route('game.goToSetScoreScreen', $game->id) }}">Score zetten</a>
        </div>
        <div class="row justify-content-md-center h2">Info</div>
    </div>
    <div class="col">
        <div class="row">
            <ul class="list-group">
                <li class="list-group-item">Spelers: {{ $gameData['players'] }} </li>
                <li class="list-group-item">Aangemaakt door: {{ $gameData['createdBy'] }}</li>
                <li class="list-group-item">Aangemaakt: {{ $gameData['dateOfCreation'] }}</li>
                <li class="list-group-item">Duur: {{ $gameData['totalGameDuration'] }}</li>
            </ul>
        </div>
        <div class="row">
            <a class="btn btn-outline-secondary" href="{{ route('game.edit', $game->id) }}">Bewerken</a>
            {{ Form::open(['route'=> ['game.destroy', $game->id], 'onsubmit'=> "return confirm('Weet je het zeker?')", 'method'=>'POST']) }}
            {{ method_field('DELETE') }}
            {{ Form::submit('Verwijderen', ['class'=>'btn btn-outline-danger', 'id' => 'delete_button'])}}
            {{ Form::close()}}
        </div>
    </div>
</div>
@endsection