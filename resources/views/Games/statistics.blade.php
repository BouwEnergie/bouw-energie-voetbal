@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-md-center">
        <div class="h1">Game {{ $game->id }}</div>
    </div>
    <div class="row justify-content-md-center">
        <div class="col; h6">{{ $totalGameDuration }}</div>
        <div class="row justify-content-md-center">
            <div class="col; h5">
                <div class="row">A: {{ $game->teamOneAttackPlayerOneId->fullname ?? 'Niet gevonden'}}</div>
                <div class="row">V: {{ $game->teamOneDefendPlayerTwoId->fullname }}</div>
            </div>
            <div class="col col-lg-2; h2">{{ $score }}</div>
            <div class="col; h5">
                <div class="row">A: {{ $game->teamTwoAttackPlayerThreeId->fullname ?? 'Niet gevonden'}}</div>
                <div class="row">V: {{ $game->teamTwoDefendPlayerFourId->fullname ?? 'Niet gevonden'}}</div>
            </div>
        </div>
        <div class="col; h6">{{ $dateOfGame }}</div>
    </div>
    <div class="row">
        <div class="row justify-content-md-center">
            <div class="h1">Spelers</div>
        </div>
        @foreach ($players as $player)
        <div class="col-sm">
            <div> {{ $player->fullname }} </div>
            <div class="card" style="width: 14rem;">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Goals: {{ count($goalStats[$player->id]['goal']) ?? '0' }} </li>
                    <li class="list-group-item">Eigen goal: {{ count($goalStats[$player->id]['ownGoal']) ?? '0' }} </li>
                    <li class="list-group-item">Middenveld goal: {{ count($goalStats[$player->id]['midfieldGoal']) ?? '0' }}</li>
                </ul>
            </div>
        </div>
        @endforeach
        <div class="row justify-content-md-center">
            <div class="row">
                <div class="h1">Algemeen</div>
                <div class="card" style="width: 14rem;">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Totaal goals: {{ count($generalStats['totalGoals']) ?? '0' }} </li>
                        <li class="list-group-item">Totaal eigen goals: {{ count($generalStats['totalOwnGoals']) ?? '0' }}</li>
                        <li class="list-group-item">Totaal middenveld goals: {{ count($generalStats['totalMidfieldGoals']) ?? '0' }}</li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div>
        <ol class="list-group list-group-flush">
            <div class="row">
                <div class="col">Team 1</div>
                <div class="col"></div>
                <div class="col">Team 2</div>
            </div>
            <div class="row">
                <div class="col">{{ count($statisticsTeamOne['ownGoals']) ?? '0' }}</div>
                <div class="col">
                    Eigen goals
                    <div class="row">
                        <div class="col">
                            <div class="progress">
                                <div class="progress-bar w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="progress">
                                <div class="progress-bar w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">{{ count($statisticsTeamTwo['ownGoals']) ?? '0' }}</div>
            </div>
            <div class="row">
                <div class="col">{{ count($statisticsTeamOne['midfieldGoals']) ?? '0' }}</div>
                <div class="col">
                    Middenveld goals
                    <div class="row">
                        <div class="col">
                            <div class="progress">
                                <div class="progress-bar w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="progress">
                                <div class="progress-bar w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">{{ count($statisticsTeamTwo['midfieldGoals']) ?? '0' }}</div>
            </div>
            <div class="row">
                <div class="col">...</div>
                <div class="col">
                    Gemmidelde tijd per goal
                    <div class="row">
                        <div class="col">
                            <div class="progress">
                                <div class="progress-bar w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="progress">
                                <div class="progress-bar w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">...</div>
            </div>
            <div class="row">
                <div class="col">{{ $statisticsTeamOne['winChance'][0] ?? '0' }} %</div>
                <div class="col">
                    Winkans %
                    <div class="row">
                        <div class="col">
                            <div class="progress">
                                <div class="progress-bar w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="progress">
                                <div class="progress-bar w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">{{ $statisticsTeamTwo['winChance'][0] ?? '0' }} %</div>
            </div>
        </ol>
    </div>
</div>
@endsection