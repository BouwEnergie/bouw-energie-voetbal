@push('javascript')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
         
        $(document).ready(function(e) {

            function checkGoal(player_id, goal_type = false) {
                own_goal = false;
                midfield_goal = false;

                if (goal_type == 'own_goal') {
                    own_goal = true;
                } else if (goal_type == 'midfield_goal') {
                    midfield_goal = true;
                }

                $.ajax({
                    url : "{{ route('game.processNewGoal') }}",
                    type : 'POST',
                    data : {
                        game_id: {{ $game->id }},
                        player_id: player_id,
                        own_goal: own_goal,
                        midfield_goal: midfield_goal,
                    },
                    success : function(response) {
                        var responseData = $.parseJSON(response);
                        console.log(responseData);
                        var saveMessage = document.getElementById("saveMessage");
                        var playerOneMidfieldGoalsPlusButton = $('#player_one_midfield_goals_plus_button');
                        var playerThreeMidfieldGoalsPlusButton = $('#player_three_midfield_goals_plus_button');

                        if (responseData.team_one_total_score == 10 || responseData.team_two_total_score == 10) {
                            saveMessage.style.display = "block";
                        }

                        $('#closeButtonSaveMessage').click(function() {
                            saveMessage.style.display = "none";
                        });
                        $('#cancelButtonSaveMessage').click(function() {
                            saveMessage.style.display = "none";
                        });
                        
                        $("#teamOne").text(responseData.team_one_total_score);
                        $("#teamTwo").text(responseData.team_two_total_score);
                    }
                });
            }
            // Speler 1 aanval goals minus button
            $('#player_one_attack_goals_minus_button').click(function() {
                var player_id = {{ $game->team_one_attack_player_one_id }};
                checkGoal(player_id);
            });

            // Speler 1 aanval goals plus button
            $('#player_one_attack_goals_plus_button').click(function() {
                var player_id = {{ $game->team_one_attack_player_one_id }};
                checkGoal(player_id);
            });

            // Speler 1 aanval eigen goals minus button
            $('#player_one_attack_own_goals_minus_button').click(function() {
                var goal_type = 'own_goal';
                var player_id = {{ $game->team_one_attack_player_one_id }};
                checkGoal(player_id, goal_type);
            });

            // Speler 1 aanval eigen goals plus button
            $('#player_one_attack_own_goals_plus_button').click(function() {
                var goal_type = 'own_goal';
                var player_id = {{ $game->team_one_attack_player_one_id }};
                checkGoal(player_id, goal_type);
            });

            // Speler 1 middenveld goals plus button
            $('#player_one_midfield_goals_plus_button').click(function() {
                var goal_type = 'midfield_goal';
                var player_id = {{ $game->team_one_attack_player_one_id }};
                checkGoal(player_id, goal_type);
            });

            // Speler 2 verdediging goals minus button
            $('#player_two_defend_goals_minus_button').click(function() {
                var player_id = {{ $game->team_one_defend_player_two_id }};
                checkGoal(player_id);
            });

            // Speler 2 verdediging goals plus button
            $('#player_two_defend_goals_plus_button').click(function() {
                var player_id = {{ $game->team_one_defend_player_two_id }};
                checkGoal(player_id);
            });

            // Speler 2 verdediging eigen goals minus button
            $('#player_two_defend_own_goals_minus_button').click(function() {
                var goal_type = 'own_goal';
                var player_id = {{ $game->team_one_defend_player_two_id }};
                checkGoal(player_id, goal_type);
            });

            // Speler 2 verdediging eigen goals plus button
            $('#player_two_defend_own_goals_plus_button').click(function() {
                var goal_type = 'own_goal';
                var player_id = {{ $game->team_one_defend_player_two_id }};
                checkGoal(player_id, goal_type);
            });

            // Speler 3 aanval goals minus button
            $('#player_three_attack_goals_minus_button').click(function() {
                var player_id = {{ $game->team_two_attack_player_three_id }};
                checkGoal(player_id);
            });

            // Speler 3 aanval goals plus button
            $('#player_three_attack_goals_plus_button').click(function() {
                var player_id = {{ $game->team_two_attack_player_three_id }};
                checkGoal(player_id);
            });
            
            // Speler 3 aanval eigen goals minus button
            $('#player_three_attack_own_goals_minus_button').click(function() {
                var goal_type = 'own_goal';
                var player_id = {{ $game->team_two_attack_player_three_id }};
                checkGoal(player_id, goal_type);
            });

            // Speler 3 aanval eigen goals plus button
            $('#player_three_attack_own_goals_plus_button').click(function() {
                var goal_type = 'own_goal';
                var player_id = {{ $game->team_two_attack_player_three_id }};
                checkGoal(player_id, goal_type);
            });

            // Speler 3 middenveld goals plus button
            $('#player_three_midfield_goals_plus_button').click(function() {
                var goal_type = 'midfield_goal';
                var player_id = {{ $game->team_two_attack_player_three_id }};
                checkGoal(player_id, goal_type);
            });

            // Speler 4 verdediging goals minus button
            $('#player_four_defend_goals_minus_button').click(function() {
                var player_id = {{ $game->team_two_defend_player_four_id }};
                checkGoal(player_id);
            });

            // Speler 4 verdediging goals plus button
            $('#player_four_defend_goals_plus_button').click(function() {
                var player_id = {{ $game->team_two_defend_player_four_id }};
                checkGoal(player_id);
            });

            // Speler 4 verdediging eigen goals minus button
            $('#player_four_defend_own_goals_minus_button').click(function() {
                var goal_type = 'own_goal';
                var player_id = {{ $game->team_two_defend_player_four_id }};
                checkGoal(player_id, goal_type);
            });

            // Speler 4 verdediging eigen goals plus button
            $('#player_four_defend_own_goals_plus_button').click(function() {
                var goal_type = 'own_goal';
                var player_id = {{ $game->team_two_defend_player_four_id }};
                checkGoal(player_id, goal_type);
            });
        });
    </script>
@endpush
    @if (session ('error'))
        <div style="display: flex" class="alert alert-danger">
            <strong class="text-center">{{ session('error') }}</strong>
            <div style="display: flex; justify-content: right">
                <button type="button" class="close" data-dismiss="alert">×</button>
            </div>
        </div>
    @endif
    @if(isset($game))
        {{ Form::open(['route' => 'game.redirectFromSetScoreScreen']) }}
    @endif
            <div class="row justify-content-md-center">
                <div>
                    <span id="teamOne"></span>
                    -
                    <span id="teamTwo"></span>
                </div>
                <div class="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
                 id="saveMessage" style="display: none;">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Wilt u deze score opslaan?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeButtonSaveMessage">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-footer">
                                {{ Form::submit('Opslaan', ['class' => 'btn btn-primary', 'name' => 'submit']) }}
                                <button type="button" class="btn btn-secondary" id="cancelButtonSaveMessage">Annuleer</button>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-sm">Team 1
                    <div class="row">{{ $game->teamOneAttackPlayerOneId->fullname ?? 'onbekend'}} (aanval)
                    </div>
                    {{ Form::label('player_one_attack_goals', 'Aanval goals')}}
                    {{ Form::button('-',  ['class' => 'btn btn-danger', 'id' => 'player_one_attack_goals_minus_button']) }}
                    {{ Form::button('+', ['class' => 'btn btn-success', 'id' => 'player_one_attack_goals_plus_button']) }}
                    <br>

                    {{ Form::label('player_one_attack_own_goals', 'Aanval eigen goals')}}
                    {{ Form::button('-', ['class' => 'btn btn-danger', 'id' => 'player_one_attack_own_goals_minus_button']) }}
                    {{ Form::button('+', ['class' => 'btn btn-success', 'id' => 'player_one_attack_own_goals_plus_button']) }}
                    <br>

                    {{ Form::label('player_one_midfield_goals', 'Middenveld goals')}}
                    {{ Form::button('+', ['class' => 'btn btn-success', 'id' => 'player_one_midfield_goals_plus_button']) }}
                    <br>

                    <br>
                    <div class="row">{{ $game->teamOneDefendPlayerTwoId->fullname ?? 'onbekend'}} (verdediging)
                    </div>
                    {{ Form::label('player_two_defend_goals', 'Verdediging goals')}}
                    {{ Form::button('-', ['class' => 'btn btn-danger', 'id' => 'player_two_defend_goals_minus_button']) }}
                    {{ Form::button('+', ['class' => 'btn btn-success', 'id' => 'player_two_defend_goals_plus_button']) }}
                    <br>

                    {{ Form::label('player_two_defend_own_goals', 'Verdediging eigen goals')}}
                    {{ Form::button('-', ['class' => 'btn btn-danger', 'id' => 'player_two_defend_own_goals_minus_button']) }}
                    {{ Form::button('+', ['class' => 'btn btn-success', 'id' => 'player_two_defend_own_goals_plus_button']) }}
                    <br>
                </div>
                <div class="col-sm">Team 2
                    <div class="row">{{ $game->teamTwoAttackPlayerThreeId->fullname ?? 'onbekend'}} (aanval)
                    </div>
                    {{ Form::label('player_three_attack_goals', 'Aanval goals')}}
                    {{ Form::button('-',  ['class' => 'btn btn-danger', 'id' => 'player_three_attack_goals_minus_button']) }}
                    {{ Form::button('+', ['class' => 'btn btn-success', 'id' => 'player_three_attack_goals_plus_button']) }}
                    <br>

                    {{ Form::label('player_three_attack_own_goals', 'Aanval eigen goals')}}
                    {{ Form::button('-', ['class' => 'btn btn-danger', 'id' => 'player_three_attack_own_goals_minus_button']) }}
                    {{ Form::button('+', ['class' => 'btn btn-success', 'id' => 'player_three_attack_own_goals_plus_button']) }}
                    <br>

                    {{ Form::label('player_three_midfield_goals', 'Middenveld goals')}}
                    {{ Form::button('+', ['class' => 'btn btn-success', 'id' => 'player_three_midfield_goals_plus_button']) }}
                    <br>

                    <br>
                    <div class="row">{{ $game->teamTwoDefendPlayerFourId->fullname ?? 'onbekend'}} (verdediging)</div>
                    {{ Form::label('player_four_defend_goals', 'Verdediging goals')}}
                    {{ Form::button('-', ['class' => 'btn btn-danger', 'id' => 'player_four_defend_goals_minus_button']) }}
                    {{ Form::button('+', ['class' => 'btn btn-success', 'id' => 'player_four_defend_goals_plus_button']) }}
                    <br>

                    {{ Form::label('player_four_defend_own_goals', 'Verdediging eigen goals')}}
                    {{ Form::button('-', ['class' => 'btn btn-danger', 'id' => 'player_four_defend_own_goals_minus_button']) }}
                    {{ Form::button('+', ['class' => 'btn btn-success', 'id' => 'player_four_defend_own_goals_plus_button']) }}
                    <br>
                </div>
            </div>
        {{ Form::close() }}
