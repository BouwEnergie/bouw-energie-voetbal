@extends('layouts.app')
@section('content')
    <div>
        @if (session('success'))
            <div style="display: flex" class="alert alert-success">
                <strong class="text-center">{{ session('success') }}</strong>
                <div style="display: flex; justify-content: right">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                </div>
            </div>
        @endif
        <div style="padding-left: 3.5rem; height: 70px">
            <a href="{{ route('game.create')}}" ; class="btn btn-success btn-lg shadow-sm">Nieuwe wedstrijd</a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col col-lg-2 font-weight-bold">Wedstrijd</div>
                <div class="col col-lg-2 font-weight-bold">Aangemaakt door</div>
                <div class="col col-lg-2 font-weight-bold">Team 1</div>
                <div class="col col-lg-2 font-weight-bold">Team 2</div>
                <div class="col col-lg-2 font-weight-bold">Score</div>
            </div>
            @foreach($games as $game)
                <div class="row">
                    <div class="col col-lg-2">
                        <a href="{{ route('game.show', $game->id) }}">{{ $game->id }}</a>
                    </div>
                    <div class="col col-lg-2">{{ $game->user->name }}</div>
                    <div class="col col-lg-2">
                        {{ $game->teamOneAttackPlayerOneId->fullname ?? 'onbekend'}},
                        {{ $game->teamOneDefendPlayerTwoId->fullname ?? 'onbekend'}}
                    </div>
                    <div class="col col-lg-2">
                        {{ $game->teamTwoAttackPlayerThreeId->fullname ?? 'onbekend'}},
                        {{ $game->teamTwoDefendPlayerFourId->fullname ?? 'onbekend'}}
                    </div>
                    <div class="col col-lg-2">
                        <span> {{ $game->team_one_total_score . ' - ' . $game->team_two_total_score}} </span>
                    </div>
                    <div class="col">
                        <div class="row">
                            <a class="btn btn-outline-info" href="{{ route('game.displayStatistics', $game->id) }}">Info</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection