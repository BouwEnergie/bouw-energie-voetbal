@extends('layouts.app')
@section('content')
    @if (session ('error'))
        <div style="display: flex" class="alert alert-danger">
            <strong class="text-center">{{ session('error') }}</strong>
            <div style="display: flex; justify-content: right">
                <button type="button" class="close" data-dismiss="alert">×</button>
            </div>
        </div>
    @endif
    @if(isset($game))
        {{ Form::model($game, ['route' => ['game.update', $game->id], 'method' => 'put']) }}
        {{ Form::hidden('id', $game->id) }}
    @else
        {{ Form::open(['route' => 'game.store', 'method' => 'post', 'id' => 'createGameForm']) }}
    @endif
            <script type="text/javascript">
                $(document).ready(function () {
                    var prevValuePlayerOne = false;
                    var prevValuePlayerTwo = false;
                    var prevValuePlayerThree= false;
                    var prevValuePlayerFour = false;

                    $("#team_one_attack_player_one_id").change(function () {
                        var dropdownPlayerOneSelectedOptionVal = $("#team_one_attack_player_one_id option:selected").val()
                        if (prevValuePlayerOne) {
                            $("#team_one_defend_player_two_id option[value='"+prevValuePlayerOne+"']").show();
                            $("#team_two_attack_player_three_id option[value='"+prevValuePlayerOne+"']").show();
                            $("#team_two_defend_player_four_id option[value='"+prevValuePlayerOne+"']").show();
                        }
                        $("#team_one_defend_player_two_id option[value='"+dropdownPlayerOneSelectedOptionVal+"']").hide();
                        $("#team_two_attack_player_three_id option[value='"+dropdownPlayerOneSelectedOptionVal+"']").hide();
                        $("#team_two_defend_player_four_id option[value='"+dropdownPlayerOneSelectedOptionVal+"']").hide();

                        prevValuePlayerOne = dropdownPlayerOneSelectedOptionVal;
                    });
                    $("#team_one_defend_player_two_id").change(function () {
                        var dropdownPlayerTwoSelectedOptionVal = $("#team_one_defend_player_two_id option:selected").val()
                        if (prevValuePlayerTwo) {
                            $("#team_one_attack_player_one_id option[value='"+prevValuePlayerTwo+"']").show();
                            $("#team_two_attack_player_three_id option[value='"+prevValuePlayerTwo+"']").show();
                            $("#team_two_defend_player_four_id option[value='"+prevValuePlayerTwo+"']").show();
                        }
                        $("#team_one_attack_player_one_id option[value='"+dropdownPlayerTwoSelectedOptionVal+"']").hide();
                        $("#team_two_attack_player_three_id option[value='"+dropdownPlayerTwoSelectedOptionVal+"']").hide();
                        $("#team_two_defend_player_four_id option[value='"+dropdownPlayerTwoSelectedOptionVal+"']").hide();

                        prevValuePlayerTwo = dropdownPlayerTwoSelectedOptionVal;
                    });
                    $("#team_two_attack_player_three_id").change(function () {
                        var dropdownPlayerThreeSelectedOptionVal = $("#team_two_attack_player_three_id option:selected").val()
                        if (prevValuePlayerThree) {
                            $("#team_one_attack_player_one_id option[value='"+prevValuePlayerThree+"']").show();
                            $("#team_one_defend_player_two_id option[value='"+prevValuePlayerThree+"']").show();
                            $("#team_two_defend_player_four_id option[value='"+prevValuePlayerThree+"']").show();
                        }
                        $("#team_one_attack_player_one_id option[value='"+dropdownPlayerThreeSelectedOptionVal+"']").hide();
                        $("#team_one_defend_player_two_id option[value='"+dropdownPlayerThreeSelectedOptionVal+"']").hide();
                        $("#team_two_defend_player_four_id option[value='"+dropdownPlayerThreeSelectedOptionVal+"']").hide();

                        prevValuePlayerThree = dropdownPlayerThreeSelectedOptionVal;
                    });
                    $("#team_two_defend_player_four_id").change(function () {
                        var dropdownPlayerFourSelectedOptionVal = $("#team_two_defend_player_four_id option:selected").val()
                        if (prevValuePlayerFour) {
                            $("#team_one_attack_player_one_id option[value='"+prevValuePlayerFour+"']").show();
                            $("#team_one_defend_player_two_id option[value='"+prevValuePlayerFour+"']").show();
                            $("#team_two_attack_player_three_id option[value='"+prevValuePlayerFour+"']").show();
                        }
                        $("#team_one_attack_player_one_id option[value='"+dropdownPlayerFourSelectedOptionVal+"']").hide();
                        $("#team_one_defend_player_two_id option[value='"+dropdownPlayerFourSelectedOptionVal+"']").hide();
                        $("#team_two_attack_player_three_id option[value='"+dropdownPlayerFourSelectedOptionVal+"']").hide();
                        
                        prevValuePlayerFour = dropdownPlayerFourSelectedOptionVal;
                    });
                });
            </script>
            <h1>Team 1</h1>
            {{ Form::label('team_one_attack_player_one_id', 'Speler 1 (aanvallend)') }}
            {{ Form::select('team_one_attack_player_one_id', $players->pluck('fullname', 'id'), Request::old('team_one_attack_player_one_id'), ['class' => 'form-control', 'placeholder' => 'Selecteer speler 1']) }}
            @error('team_one_attack_player_one_id')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            {{ Form::label('team_one_defend_player_two_id', 'Speler 2 (verdedigend)')}}
            {{ Form::select('team_one_defend_player_two_id', $players->pluck('fullname', 'id'), Request::old('team_one_defend_player_two_id'), ['class' => 'form-control', 'placeholder' => 'Selecteer speler 2']) }}
            @error('team_one_defend_player_two_id')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <h1>Team 2</h1>
            {{ Form::label('team_two_attack_player_three_id', 'Speler 3 (aanvallend)')}}
            {{ Form::select('team_two_attack_player_three_id', $players->pluck('fullname', 'id'), Request::old('team_two_attack_player_three_id'), ['class' => 'form-control', 'placeholder' => 'Selecteer speler 3']) }}
            @error('team_two_attack_player_three_id')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            {{ Form::label('team_two_defend_player_four_id', 'Speler 4 (verdedigend)')}}
            {{ Form::select('team_two_defend_player_four_id', $players->pluck('fullname', 'id'), Request::old('team_two_defend_player_four_id'), ['class' => 'form-control', 'placeholder' => 'Selecteer speler 4']) }}
            @error('team_two_defend_player_four_id')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            {{ Form::submit('Opslaan', ['class'=>'btn btn-primary', 'id' => 'save_button']) }}
        {{ Form::close() }}
@endsection