@extends('layouts.app')
@section('content')
    <div>
        @if (session('success'))
            <div style="display: flex" class="alert alert-success">
                <strong class="text-center">{{ session('success') }}</strong>
                <div style="display: flex; justify-content: right">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                </div>
            </div>
        @endif
            <div style="padding-left: 3.5rem; height: 70px">
                <a href="{{ route('user.create') }}" ; class="btn btn-success btn-lg shadow-sm">Nieuwe gebruiker</a>
            </div>
        <div class="container">
            <div class="row">
                <div class="col col-lg-2">ID</div>
                <div class="col col-lg-2">NAAM</div>
                <div class="col col-lg-2">E-MAIL</div>
                <div class="col col-lg-2">ACTIE</div>
            </div>
            @foreach($users as $user)
                <div class="row">
                    <div class="col col-lg-2">{{ $user->id }}</div>
                    <div class="col col-lg-2"><a href="{{ route('user.show', $user->id) }}" ; class="nav-link">{{ $user->name }}</div>
                    <div class="col col-lg-2"><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></div>
                    <div class="row">
                        <form action="{{ route('user.destroy', $user->id) }}" method="POST" onsubmit="return confirm('Weet je het zeker?')">
                            <a class="btn btn-outline-secondary" href="{{ route('user.edit', $user->id) }}">Bewerken</a>
                            @csrf
                            @method('DELETE')
                            <button name="btn" id="delete_button" type="submit" class="btn btn-outline-danger">Verwijder</button>
                        </form>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection