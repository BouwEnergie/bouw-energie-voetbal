@extends('layouts.app')
@section('content')
    <div style="text-align: center;">
        <h1 class="shadow-sm p-3 mb-5 bg-white rounded">Weergave gebruiker</h1>
    </div>
    <div style="display: flex; justify-content: center;">
        <div class="card text-center shadow p-3 mb-5 bg-white rounded" style="display: inline-flex; width: 30rem">
            <img class="card-img-top" src="" alt="Profiel foto">
            <div class="card-body" style="height: 20rem;">
                <h1 style="font-size: 35px;" class="card-title">{{ $user->name }}</h1>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">E-mailadres: &nbsp;<a href="mailto:{{ $user->email }}">{{ $user->email }}</a></li>
                <li class="list-group-item">Admin status: &nbsp; {{ $user->is_admin }}</li>
                <li class="list-group-item"></li>
            </ul>
            <div class="card-body" style="display: flex; justify-content: center;">
                <a href="{{ route('user.index') }}" class="btn btn-primary btn-lg">Terug</a>
            </div>
        </div>
    </div>
@endsection