<!DOCTYPE html>
@extends('layouts.app')
@section('content')
    @if(isset($user))
        {{ Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'put']) }}
        {{ Form::hidden('id', $user->id) }}
        @if (session ('error'))
        <div style="display: flex" class="alert alert-danger">
                <strong class="text-center">{{ session('error') }}</strong>
                <div style="display: flex; justify-content: right">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                </div>
            </div>
        @endif
    @else
        {{ Form::open(['route' => 'user.store', 'method' => 'post']) }}
        @if (session ('error'))
        <div style="display: flex" class="alert alert-danger">
                <strong class="text-center">{{ session('error') }}</strong>
                <div style="display: flex; justify-content: right">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                </div>
            </div>
        @endif
    @endif
        <label for="name">Naam:</label>
        {{ Form::text('name', Request::old('name')) }}
        @if ($errors->has('name'))
            <span class="text-danger">{{ $errors->first('name') }}</span>
        @endif
        <label for="email">E-mail:</label>
        {{ Form::email('email', Request::old('email')) }}
        @if ($errors->has('email'))
            <span class="text-danger">{{ $errors->first('email') }}</span> 
        @endif
        
    @if(!isset($user))
        <label for="password">Wachtwoord:</label>
        {{ Form::password('password', Request::old('password')) }}
        @if ($errors->has('password'))
            <span class="text-danger">{{ $errors->first('password') }}</span> 
        @endif
        
    @endif
        {{ Form::submit('Opslaan', ['class'=>'btn btn-primary', 'id' => 'save_button']) }}
        {{ Form::close() }}
@endsection