@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col h1">Dagen</div>
        <div class="col h1">Wedstrijden</div>
        <div class="col h1">Posities</div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card" style="width: 18rem;">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Gemmidelde aantal wedstrijden: {{ $allDayStats['avgGames'] }}</li>
                    <li class="list-group-item">Gemiddelde aantal goals: {{ $allDayStats['avgGoals'] }}</li>
                    <li class="list-group-item">Gemiddelde speelduur: {{ $allDayStats['avgPlayTime'] }}</li>
                </ul>
            </div>
        </div>
        <div class="col">
            <div class="card" style="width: 18rem;">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Totaal gespeelde wedstrijden: {{ $allGamesStats['allGames'] }}</li>
                    <li class="list-group-item">Totaal gemaakte goals: {{ $allGamesStats['allGoals'] }}</li>
                    <li class="list-group-item">Totaal gemaakte eigen goals: {{ $allGamesStats['allOwnGoals'] }}</li>
                    <li class="list-group-item">Totaal gemaakte middenveld goals: {{ $allGamesStats['allMidfieldGoals'] }}</li>
                    <li class="list-group-item">Gemiddelde goals: {{ $allGamesStats['avgGoalsPerGame'] }}</li>
                    <li class="list-group-item">Gemiddelde eigen goals: {{ $allGamesStats['avgOwnGoalsPerGame'] }}</li>
                    <li class="list-group-item">Gemiddelde middenveld goals: {{ $allGamesStats['avgMidfieldGoalsPerGame'] }}</li>
                    <li class="list-group-item">Gemiddelde speelduur: {{ $allGamesStats['avgPlaytimePerGame'] }}</li>
                </ul>
            </div>
        </div>
        <div class="col">
            <div class="card" style="width: 18rem;">
                <div class="h4">Aanval</div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Totaal goals: {{ $allAttackStats['totalGoalsAttack'] }}</li>
                    <li class="list-group-item">Totaal eigen goals: {{ $allAttackStats['totalOwnGoalsAttack'] }}</li>
                    <li class="list-group-item">Totaal middeveld goals: {{ $allAttackStats['totalMidfieldGoalsAttack']}}</li>
                    <li class="list-group-item">Gemiddelde goals: {{ $allAttackStats['avgGoalsAttack'] }}</li>
                </ul>
            </div>
            <div class="card" style="width: 18rem;">
                <div class="h4">Verdediging</div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Totaal goals: {{ $allDefendStats['totalGoalsDefend'] }}</li>
                    <li class="list-group-item">Totaal eigen goals: {{ $allDefendStats['totalOwnGoalsDefend'] }}</li>
                    <li class="list-group-item">Gemiddelde goals: {{ $allDefendStats['avgGoalsDefend'] }}</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection