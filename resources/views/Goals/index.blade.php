@extends('layouts.app')
@section('content')
    <div>
        @if (session('success'))
            <div style="display: flex" class="alert alert-success">
                <strong class="text-center">{{ session('success') }}</strong>
                <div style="display: flex; justify-content: right">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                </div>
            </div>
        @endif
        <div style="padding-left: 3.5rem; height: 70px">
            <a href="{{ route('goal.create')}}" ; class="btn btn-success btn-lg shadow-sm">Nieuw goal</a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col col-lg-2">GOALS</div>
            </div>
            @foreach($goals as $goal)
                <div class="row">
                    <div class="col col-lg-2"><a href="{{ route('goal.show', $goal->id) }}">{{ $goal->id}}</a></div>
                    <div class="col">
                        <div class="row">
                            <a class="btn btn-outline-info" href="{{ route('goal.edit', $goal->id) }}">Bewerken</a>
                            {{ Form::open(['route'=> ['goal.destroy', $goal->id], 'method'=>'POST']) }}
                            {{ method_field('DELETE') }}
                            {{ Form::submit('Verwijderen', ['class'=>'btn btn-outline-danger'])}}
                            {{ Form::close()}}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection