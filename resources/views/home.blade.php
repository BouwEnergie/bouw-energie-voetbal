@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="h2">Recente wedstrijd</div>
        <div class="row justify-content-md-center">
            <div class="col; h6">{{ $recentGameData['recentGameDuration'] }}</div>
            <div class="row justify-content-md-center">
                <div class="col; h5">
                    <div class="row">A: {{ $recentGame->teamOneAttackPlayerOneId->fullname ?? 'Niet gevonden'}}</div>
                    <div class="row">V: {{ $recentGame->teamOneDefendPlayerTwoId->fullname }}</div>
                </div>
                <div class="col col-lg-2; h2">{{ $recentGameData['score'] }}</div>
                <div class="col; h5">
                    <div class="row">A: {{ $recentGame->teamTwoAttackPlayerThreeId->fullname ?? 'Niet gevonden'}}</div>
                    <div class="row">V: {{ $recentGame->teamTwoDefendPlayerFourId->fullname ?? 'Niet gevonden'}}</div>
                </div>
            </div>
            <div class="col; h6">{{ $recentGameData['recentGameDate'] }}</div>
        </div>
    </div>
    <div class="col">
        <div class="h2">Laatste wedstrijden</div>
        @foreach($lastFiveGames as $game)
        <div class="row">
            <ul class="list-group">
                <li class="list-group-item">
                    <div class="col; h5">
                        <div class="row">A: {{ $game->teamOneAttackPlayerOneId->fullname ?? 'Niet gevonden' }}</div>
                        <div class="row">V: {{ $game->teamOneDefendPlayerTwoId->fullname ?? 'Niet gevonden' }}</div>
                    </div>
                    <div class="col col-lg-2; h2">{{ $game->team_one_total_score . ' - ' . $game->team_two_total_score}}</div>
                    <div class="col; h5">
                        <div class="row">A: {{ $game->teamTwoAttackPlayerThreeId->fullname ?? 'Niet gevonden' }}</div>
                        <div class="row">V: {{ $game->teamTwoDefendPlayerFourId->fullname ?? 'Niet gevonden' }}</div>
                    </div>
                </li>
            </ul>
        </div>
        @endforeach
    </div>
    <div class="col">
        <div class="h2">Topscorers (kwartaal)</div>
        @foreach($topFiveTopScorers as $topScorers)
        <ul class="list-group">
            <li class="list-group-item">{{ $topScorers['player']->fullname . ' ' . $topScorers['goals'] }} </li>
        </ul>
        @endforeach
        
    </div>
</div>
@endsection