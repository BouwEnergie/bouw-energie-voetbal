<?php

use App\Http\Controllers\GameController;
use App\Http\Controllers\GeneralStatisticsController;
use App\Http\Controllers\GoalController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PlayerController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => true]);

Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', [HomeController::class, 'index'])->name('home');

    Route::resource('user', UserController::class);
    Route::resource('player', PlayerController::class);
    Route::resource('game', GameController::class);
    Route::post('/game/processNewGoal', [GameController::class, 'processNewGoal'])
        ->name('game.processNewGoal');
    Route::post('/game/redirectFromSetScoreScreen', [GameController::class, 'redirectFromSetScoreScreen'])
        ->name('game.redirectFromSetScoreScreen');
    Route::get('/game/{id}/displayStatistics', [GameController::class, 'displayStatistics'])
        ->name('game.displayStatistics');
    Route::get('/player/{id}/displayStatistics', [PlayerController::class, 'displayStatistics'])
        ->name('player.displayStatistics');
    Route::get('/game/{id}/goToSetScoreScreen', [GameController::class, 'goToSetScoreScreen'])
        ->name('game.goToSetScoreScreen');
    Route::resource('generalStatistics', GeneralStatisticsController::class);
    
   
    Route::resource('goal', GoalController::class);
});
