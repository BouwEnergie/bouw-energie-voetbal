<?php

namespace App\Http\Controllers;

use App\Http\Requests\GameRequest;
use App\Models\Game;
use App\Models\Goal;
use App\Models\Player;
use App\Models\User;
use Auth;
use DB;
use Doctrine\DBAL\Query\QueryException;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $games = Game::all();
        
        return view('Games.index')
            ->with('games', $games);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $players = Player::all();
        return view('Games.createOrUpdate')
            ->with('players', $players);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  GameRequest  $request
     * @return RedirectResponse
     */
    public function store(GameRequest $request): RedirectResponse
    {
        try {
            $game = new Game();
            $game->team_one_attack_player_one_id = $request->team_one_attack_player_one_id;
            $game->team_one_defend_player_two_id = $request->team_one_defend_player_two_id;
            $game->team_two_attack_player_three_id = $request->team_two_attack_player_three_id;
            $game->team_two_defend_player_four_id = $request->team_two_defend_player_four_id;
            $game->user_id = Auth::user()->id;
            $game->save();
        } catch (QueryException $e) {
            session()->flash('error', 'Wedstrijd aanmaken mislukt');
            return redirect()->back();
        } catch (Exception $e) {
            session()->flash('error', 'Wedstrijd aanmaken mislukt');
            return redirect()->back();
        }
        return redirect()
            ->route('game.index')
            ->with('success', 'Wedstrijd aangemaakt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  Game  $game
     * @return View
     */
    public function show(Game $game): View
    {
        $gameData = [
            'players' =>
            $game->teamOneAttackPlayerOneId->fullname . ', ' .
            $game->teamOneAttackPlayerOneId->fullname . ', ' .
            $game->teamOneDefendPlayerTwoId->fullname . ', ' .
            $game->teamTwoAttackPlayerThreeId->fullname . ', ' .
            $game->teamTwoDefendPlayerFourId->fullname . ', ' ,
            'createdBy' => $game->user_id,
            'dateOfCreation' => $game->dateOfGame(),
            'totalGameDuration' => $game->calculateGameDuration()->format("%I:%S"),
        ];

        return view('Games.show')
            ->with('game', $game)
            ->with('gameData', $gameData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  game  $game
     * @return View
     */
    public function edit(Game $game): View
    {
        $players = Player::all();
        return view('Games.createOrUpdate')
            ->with('game', $game)
            ->with('players', $players);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Game  $game
     * @param  GameRequest  $request
     * @return RedirectResponse
     */
    public function update(GameRequest $request, Game $game): RedirectResponse
    {
        try {
            $game->team_one_attack_player_one_id = $request['team_one_attack_player_one_id'];
            $game->team_one_defend_player_two_id = $request['team_one_defend_player_two_id'];
            $game->team_two_attack_player_three_id = $request['team_two_attack_player_three_id'];
            $game->team_two_defend_player_four_id = $request['team_two_defend_player_four_id'];
            $game->user_id = Auth::user()->id;
            $game->save();
        } catch (QueryException $e) {
            session()->flash('error', 'Wedstrijd bijwerken mislukt');
            return redirect()->back();
        } catch (Exception $e) {
            session()->flash('error', 'Wedstrijd bijwerken mislukt');
            return redirect()->back();
        }

        session()->flash('success', 'Wedstrijd bijgewerkt');

        return redirect()
            ->route('game.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Game  $game
     * @return Response
     */
    public function destroy(Game $game): RedirectResponse
    {
        $game->delete();
        session()->flash('success', 'Wedstrijd verwijderd!');
        return redirect()
            ->route('game.index');
    }

    public function processNewGoal(Request $request)
    {
        $game = Game::findOrFail($request->game_id);

        $newGoal = new Goal();
        $newGoal->game_id = $game->id;
        $newGoal->player_id = $request->player_id;
        $newGoal->own_goal = $request->boolean('own_goal');
        $newGoal->midfield_goal = $request->boolean('midfield_goal');
        $newGoal->save();
        
        $game->calculateNewGoal($newGoal);
        $game->save();
        return $game->toJson();
    }

    public function redirectFromSetScoreScreen(): RedirectResponse
    {
        session()->flash('success', 'Score toegevoegd!');
        return redirect()
            ->route('game.index');
    }
    
    public function displayStatistics(Request $request): View
    {
        $game = Game::findOrFail($request->route('id'));
        $players = [
            $game->teamOneAttackPlayerOneId,
            $game->teamOneDefendPlayerTwoId,
            $game->teamTwoAttackPlayerThreeId,
            $game->teamTwoDefendPlayerFourId,
        ];
        
        $score = $game->team_one_total_score . ' - ' . $game->team_two_total_score;

        return view('Games.statistics')
            ->with('score', $score)
            ->with('statisticsTeamOne', $game->statisticsTeamOne())
            ->with('statisticsTeamTwo', $game->statisticsTeamTwo())
            ->with('goalStats', $game->getStatistics())
            ->with('totalGameDuration', $game->calculateGameDuration()->format("%I:%S"))
            ->with('dateOfGame', $game->dateOfGame())
            ->with('generalStats', $game->generalGameStats())
            ->with('game', $game)
            ->with('players', $players);
    }

    public function goToSetScoreScreen(int $id): View
    {
        $game = Game::findOrFail($id);

        return view('Games.setScoreScreen')
            ->with('game', $game);
    }
}
