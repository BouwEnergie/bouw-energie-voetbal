<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayerRequest;
use App\Models\Player;
use Doctrine\DBAL\Query\QueryException;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class PlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): View
    {
        $player = Player::all();
        return view('players.index')->with('players', $player);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(): View
    {
        return view('players.createOrUpdate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PlayerRequest  $request
     * @return Response
     */
    public function store(PlayerRequest $request): RedirectResponse
    {
        try {
            Player::create($request->all());
        } catch (QueryException $e) {
            $request->session()->flash('error', 'Gebruiker aanmaken mislukt');
            return redirect()->back();
        } catch (Exception $e) {
            $request->session()->flash('error', 'Gebruiker aanmaken mislukt');
            return redirect()->back();
        }
        return redirect()->route('player.index')
            ->with('success', 'Gebruiker aangemaakt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  Player  $player
     * @return Response
     */
    public function show(Player $player): View
    {
        $dateOfCreation = Carbon::parse($player->created_at)->format("d-m-Y");

        $playerData = [
            'fullname' => $player->fullname,
            'dateOfBirth' => $player->date_of_birth,
            'dateOfCreation' => $dateOfCreation,
        ];

        return view('players.show')
        ->with('player', $player)
        ->with('playerData', $playerData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Player  $player
     * @return Response
     */
    public function edit(Player $player): View
    {
        return view('players.createOrUpdate')
            ->with('player', $player);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PlayerRequest  $request
     * @return Response
     */
    public function update(PlayerRequest $request): RedirectResponse
    {
        try {
            $player = Player::findOrFail($request->id);
            $player->update([
                'firstname' => $request->firstname,
                'surname' => $request->surname,
                'date_of_birth' => $request->date_of_birth,
            ]);
        } catch (QueryException $e) {
            $request->session()->flash('error', 'Gebruiker bijwerken mislukt');
            return redirect()->back();
        } catch (\Exception $e) {
            $request->session()->flash('error', 'Gebruiker bijwerken mislukt');
            return redirect()->back();
        }
        $request->session()->flash('success', 'Gebruiker bijgewerkt');
        return redirect()->route('player.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Player  $player
     * @return Response
     */
    public function destroy(Player $player): RedirectResponse
    {
        $player->delete();
        return redirect()->route('player.index')
            ->with('success', 'Gebruiker verwijderd!');
    }

    public function displayStatistics(Request $request): View
    {
        $player = Player::findOrFail($request->route('id'));
        
        if ($player->countTotalGoalsAttack() > 0 || $player->countTotalGoalsDefend() > 0 || $player->countTotalGamesAttack() > 0
         || $player->countTotalGamesDefend() > 0) {
            $avgGoalsPerGameTotal = round(($player->countTotalGoalsAttack() + $player->countTotalGoalsDefend())
            / ($player->countTotalGamesAttack() + $player->countTotalGamesDefend()), 2);
        } else {
             $avgGoalsPerGameTotal = 0;
        }

        if ($player->countTotalOwnGoalsAttack() > 0 || $player->countTotalOwnGoalsDefend() > 0 || $player->countTotalGamesAttack() > 0
        || $player->countTotalGamesDefend() > 0) {
            $avgOwnGoalsPerGameTotal = round(($player->countTotalOwnGoalsAttack() + $player->countTotalOwnGoalsDefend())
            / ($player->countTotalGamesAttack() + $player->countTotalGamesDefend()), 2);
        } else {
            $avgOwnGoalsPerGameTotal = 0;
        }

        if ($player->countTotalMidfieldGoalsAttack() > 0 || $player->countTotalGoalsAttack() > 0) {
            $avgMidfieldGoalsPerGameTotal = round($player->countTotalMidfieldGoalsAttack()
            / $player->countTotalGoalsAttack(), 2);
        } else {
            $avgMidfieldGoalsPerGameTotal = 0;
        }

        $playerStatsTotal = [
            'playerGamesTotal' => $player->countTotalGamesAttack() + $player->countTotalGamesDefend(),
            'playerGoalsTotal' => $player->countTotalGoalsAttack() + $player->countTotalGoalsDefend(),
            'playerOwnGoalsTotal' => $player->countTotalOwnGoalsAttack() + $player->countTotalOwnGoalsDefend(),
            'playerMidfieldGoalsTotal' => $player->countTotalMidfieldGoalsAttack(),
            'avgGoalsPerGameTotal' => $avgGoalsPerGameTotal,
            'avgOwnGoalsPerGameTotal' => $avgOwnGoalsPerGameTotal,
            'avgMidfieldGoalsPerGameTotal' => $avgMidfieldGoalsPerGameTotal,
            'winPercentageTotal' => $player->winPercentageTotal(),
            'gameAppearanceTotal' => $player->playerGameAppearanceTotal(),
        ];

        if ($player->countTotalGoalsAttack() > 0 || $player->countTotalGamesAttack() > 0) {
            $avgGoalsPerGameAttack = round($player->countTotalGoalsAttack() / $player->countTotalGamesAttack(), 2);
        } else {
            $avgGoalsPerGameAttack = 0;
        }

        if ($player->countTotalOwnGoalsAttack() > 0 || $player->countTotalGamesAttack() > 0) {
            $avgOwnGoalsPerGameAttack = round($player->countTotalOwnGoalsAttack() / $player->countTotalGamesAttack(), 2);
        } else {
            $avgOwnGoalsPerGameAttack = 0;
        }

        if ($player->countTotalMidfieldGoalsAttack() > 0 || $player->countTotalGamesAttack() > 0) {
            $avgMidfieldGoalsPerGameAttack = round($player->countTotalMidfieldGoalsAttack() / $player->countTotalGamesAttack(), 2);
        } else {
            $avgMidfieldGoalsPerGameAttack = 0;
        }

        $playerStatsAttack = [
            'playerGamesAttack' => $player->countTotalGamesAttack(),
            'playerGoalsAttack' => $player->countTotalGoalsAttack(),
            'playerOwnGoalsAttack' => $player->countTotalOwnGoalsAttack(),
            'playerMidfieldGoalsAttack' => $player->countTotalMidfieldGoalsAttack(),
            'avgGoalsPerGameAttack' => $avgGoalsPerGameAttack,
            'avgOwnGoalsPerGameAttack' => $avgOwnGoalsPerGameAttack,
            'avgMidfieldGoalsPerGameAttack' => $avgMidfieldGoalsPerGameAttack,
            'winPercentageAttack' => $player->winPercentageAttack(),
            'gameAppearanceAttack' => $player->playerGameAppearanceAttack(),
        ];

        if ($player->countTotalGoalsDefend() > 0 || $player->countTotalGamesDefend() > 0) {
            $avgGoalsPerGameDefend = round($player->countTotalGoalsDefend() / $player->countTotalGamesDefend(), 2);
        } else {
            $avgGoalsPerGameDefend = 0;
        }

        if ($player->countTotalOwnGoalsDefend() > 0 || $player->countTotalGamesDefend() > 0) {
            $avgOwnGoalsPerGameDefend = round($player->countTotalOwnGoalsDefend() / $player->countTotalGamesDefend(), 2);
        } else {
            $avgOwnGoalsPerGameDefend = 0;
        }

        $playerStatsDefend = [
            'playerGamesDefend' => $player->countTotalGamesDefend(),
            'playerGoalsDefend' => $player->countTotalGoalsDefend(),
            'playerOwnGoalsDefend' => $player->countTotalOwnGoalsDefend(),
            'avgGoalsPerGameDefend' => $avgGoalsPerGameDefend,
            'avgOwnGoalsPerGameDefend' => $avgOwnGoalsPerGameDefend,
            'winPercentageDefend' => $player->winPercentageDefend(),
            'gameAppearanceDefend' => $player->playerGameAppearanceDefend(),
        ];
        
        ;
        if ($player->recentGame() == !null) {
            $recentGameCreatedTime = Carbon::parse($player->recentGame()->created_at);
            $recentGameUpdatedTime = Carbon::parse($player->recentGame()->updated_at);
            $totalRecentGameDuration = $recentGameCreatedTime->diff($recentGameUpdatedTime)->format("%I:%S");
        } else {
            $totalRecentGameDuration = 0;
        }
        
        if ($player->recentGame() == !null) {
            $recentGameDate = Carbon::parse($player->recentGame()->created_at)->format("d:m:Y");
        } else {
            $recentGameDate = 0;
        }
        
        if ($player->recentGame() == !null) {
            $score = $player->recentGame()->team_one_total_score . ' - ' . $player->recentGame()->team_two_total_score;
        } else {
            $score = 0;
        }
        

        $recentGameData = [
            'recentGameDuration' => $totalRecentGameDuration,
            'recentGameDate' => $recentGameDate,
            'score' => $score,
        ];
        
        return view('Players.statistics')
            ->with('playerStatsTotal', $playerStatsTotal)
            ->with('playerStatsAttack', $playerStatsAttack)
            ->with('playerStatsDefend', $playerStatsDefend)
            ->with('recentGame', $player->recentGame())
            ->with('recentGameData', $recentGameData)
            ->with('player', $player);
    }
}
