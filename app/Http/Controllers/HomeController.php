<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\Player;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $recentGame = Game::latest()
        ->first();

        $lastFiveGames = Game::latest()
        ->take(5)
        ->get();

        if ($recentGame == !null) {
            $recentGameCreatedTime = Carbon::parse($recentGame->created_at);
            $recentGameUpdatedTime = Carbon::parse($recentGame->updated_at);
            $totalRecentGameDuration = $recentGameCreatedTime->diff($recentGameUpdatedTime)->format("%I:%S");
        } else {
            $totalRecentGameDuration = 0;
        }
        
        if ($recentGame == !null) {
            $recentGameDate = Carbon::parse($recentGame->created_at)->format("d-m-Y");
        } else {
            $recentGameDate = 0;
        }

        $recentGameData = [
            'recentGameDuration' => $totalRecentGameDuration,
            'recentGameDate' => $recentGameDate,
            'score' => $recentGame->team_one_total_score . ' - ' . $recentGame->team_two_total_score,
        ];
        
        $mostTotalGoalsByPlayer = [];
        $players = Player::withTrashed()->get();

        $thisMonth = Carbon::now()->format('n');
        switch ($thisMonth) {
            case ($thisMonth >= 1 && $thisMonth <= 3):
                $addMonths = 0;
                break;
            case ($thisMonth >= 4 && $thisMonth <= 6):
                $addMonths = 3;
                break;
            case ($thisMonth >= 7 && $thisMonth <= 9):
                $addMonths = 6;
                break;
            case ($thisMonth >= 10 && $thisMonth <= 12):
                $addMonths = 9;
                break;
        }
        
        $quarter = Carbon::now()->startOfYear()->addMonths($addMonths);

        foreach ($players as $player) {
            $mostTotalGoalsByPlayer[] = [
                'player' => $player,
                'goals' => $player->goals()->where('created_at', '>',  $quarter)->where('midfield_goal', false)->where('own_goal', false)->count(),
                'games' => 
            ];
        }

        usort($mostTotalGoalsByPlayer, function($a, $b) {
            return $a['goals'] < $b['goals'];
        });

        $topFiveTopScorers = array_slice($mostTotalGoalsByPlayer, 0, 5);

        return view('home')
        ->with('recentGame', $recentGame)
        ->with('recentGameData', $recentGameData)
        ->with('lastFiveGames', $lastFiveGames)
        ->with('topFiveTopScorers', $topFiveTopScorers);
    }
}
