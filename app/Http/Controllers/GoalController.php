<?php

namespace App\Http\Controllers;

use App\Http\Requests\GoalRequest;
use App\Models\Goal;
use Doctrine\DBAL\Query\QueryException;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class GoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $goals = Goal::all();
        return view('goals.index')
            ->with('goals', $goals);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('goals.createOrUpdate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        try {
            Goal::create($request->all());
        } catch (QueryException $e) {
            session()->flash('error', 'Goal aanmaken mislukt');
            return redirect()->back();
        } catch (Exception $e) {
            session()->flash('error', 'Goal aanmaken mislukt');
            return redirect()->back();
        }
        

        session()->flash('success', 'Goal aangemaakt');

        return redirect()
        ->route('goal.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Goal  $goal
     * @return View
     */
    public function show(Goal $goal): View
    {
        return view('goals.show')
            ->with('goal', $goal->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Goal  $goal
     * @return Response
     */
    public function edit(Goal $goal)
    {
        return view('goals.createOrUpdate')
            ->with('goal', $goal->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Goal  $goals
     * @return Response
     */
    public function update(Request $request, Goal $goals)
    {
        try {
            $goals = Goal::findOrFail($request->id);
            $goals->update();
        } catch (QueryException $e) {
            session()->flash('error', 'Goal bijwerken mislukt');
            return redirect()->back();
        } catch (Exception $e) {
            session()->flash('error', 'Goal bijwerken mislukt');
            return redirect()->back();
        }

        session()->flash('success', 'Goal bijgewerkt');

        return redirect()
        ->route('goal.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Goal  $goal
     * @return Response
     */
    public function destroy(Goal $goal)
    {
        $goal->delete();
        session()->flash('success', 'Goal verwijderd!');
        return redirect()
        ->route('goal.index');
    }
}
