<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\Goal;
use App\Models\Player;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Carbon;

class GeneralStatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $players = Player::all();
        $goals = Goal::all();
        $games = Game::all();
        $allGamesTotalGameTime = 0;
        foreach ($games as $game) {
            $allGamesTotalGameTime = $allGamesTotalGameTime + $game->created_at->diffInSeconds($game->updated_at);
        }

        $allGoals = 0;

        foreach ($games as $game) {
            $allGoals = $allGoals + Goal::where('game_id', $game->id)
            ->where('own_goal', false)
            ->where('midfield_goal', false)->count();
        }

        $allGamesStats = [
            'allGames' => $games->count(),
            'allGoals' => $allGoals,
            'allOwnGoals' => $goals->where('own_goal', true)->count(),
            'allMidfieldGoals' => $goals->where('midfield_goal', true)->count(),
            'avgGoalsPerGame' => round($goals->count() / $games->count(), 2),
            'avgOwnGoalsPerGame' => round($goals->where('own_goal', true)->count() / $games->count(), 2),
            'avgMidfieldGoalsPerGame' => round($goals->where('midfield_goal', true)->count() / $games->count(), 2),
            'avgPlaytimePerGame' => Carbon::parse($allGamesTotalGameTime / $games->count())->format("i:s"),
        ];
        // normal goals
        $totalGoalsTeamOneAttack = 0;
        $totalGoalsTeamTwoAttack = 0;
        $totalGoalsTeamOneDefend = 0;
        $totalGoalsTeamTwoDefend = 0;

        // own goals
        $totalOwnGoalsTeamOneAttack = 0;
        $totalOwnGoalsTeamTwoAttack = 0;
        $totalOwnGoalsTeamOneDefend = 0;
        $totalOwnGoalsTeamTwoDefend = 0;

        // middeveld goals
        $totalMidfieldGoalsTeamOneAttack = 0;
        $totalMidfieldGoalsTeamTwoAttack = 0;

        // days
        $totalDaysGamesExisted = 0;

        foreach ($games as $game) {
            // normal goals
            $totalGoalsTeamOneAttack = $totalGoalsTeamOneAttack + Goal::where('game_id', $game->id)
            ->where('player_id', $game->team_one_attack_player_one_id)
            ->where('own_goal', false)
            ->where('midfield_goal', false)->count();
            $totalGoalsTeamTwoAttack = $totalGoalsTeamTwoAttack + Goal::where('game_id', $game->id)
            ->where('player_id', $game->team_two_attack_player_three_id)
            ->where('own_goal', false)
            ->where('midfield_goal', false)->count();
            $totalGoalsTeamOneDefend = $totalGoalsTeamOneDefend + Goal::where('game_id', $game->id)
            ->where('player_id', $game->team_one_defend_player_two_id)
            ->where('own_goal', false)
            ->where('midfield_goal', false)->count();
            $totalGoalsTeamTwoDefend = $totalGoalsTeamTwoDefend + Goal::where('game_id', $game->id)
            ->where('player_id', $game->team_two_defend_player_four_id)
            ->where('own_goal', false)
            ->where('midfield_goal', false)->count();

            // own goals 
            $totalOwnGoalsTeamOneAttack = $totalOwnGoalsTeamOneAttack + Goal::where('game_id', $game->id)
            ->where('player_id', $game->team_one_attack_player_one_id)
            ->where('own_goal', true)->count();
            $totalOwnGoalsTeamTwoAttack = $totalOwnGoalsTeamTwoAttack + Goal::where('game_id', $game->id)
            ->where('player_id', $game->team_two_attack_player_three_id)
            ->where('own_goal', true)->count();
            $totalOwnGoalsTeamOneDefend = $totalOwnGoalsTeamOneDefend + Goal::where('game_id', $game->id)
            ->where('player_id', $game->team_one_defend_player_two_id)
            ->where('own_goal', true)->count();
            $totalOwnGoalsTeamTwoDefend = $totalOwnGoalsTeamTwoDefend + Goal::where('game_id', $game->id)
            ->where('player_id', $game->team_two_defend_player_four_id)
            ->where('own_goal', true)->count();

            // middenveld goals
            $totalMidfieldGoalsTeamOneAttack = $totalMidfieldGoalsTeamOneAttack + Goal::where('game_id', $game->id)
            ->where('player_id', $game->team_one_attack_player_one_id)
            ->where('midfield_goal', true)->count();
            $totalMidfieldGoalsTeamTwoAttack = $totalMidfieldGoalsTeamTwoAttack + Goal::where('game_id', $game->id)
            ->where('player_id', $game->team_two_attack_player_three_id)
            ->where('midfield_goal', true)->count();
        }

        // dagen
        $firstGameCreatedAt = $game->first()->created_at;
        $todaysDate = Carbon::now();
        
        $totalDaysGamesExisted = $totalDaysGamesExisted + $firstGameCreatedAt->diffInDays($todaysDate);

        // normal goals
        $totalGoalsAttack = $totalGoalsTeamOneAttack + $totalGoalsTeamTwoAttack;
        $totalGoalsDefend = $totalGoalsTeamOneDefend + $totalGoalsTeamTwoDefend;

        // own goals
        $totalOwnGoalsAttack = $totalOwnGoalsTeamOneAttack + $totalOwnGoalsTeamTwoAttack;
        $totalOwnGoalsDefend = $totalOwnGoalsTeamOneDefend + $totalOwnGoalsTeamTwoDefend;

        // middenveld goals
        $totalMidfieldGoalsAttack = $totalMidfieldGoalsTeamOneAttack + $totalMidfieldGoalsTeamTwoAttack;
        
        // avg goals
        $avgGoalsAttack = $totalGoalsAttack / $games->count();
        $avgGoalsDefend = $totalGoalsDefend / $games->count();

        $allAttackStats = [
            'totalGoalsAttack' => $totalGoalsAttack,
            'totalOwnGoalsAttack' => $totalOwnGoalsAttack,
            'totalMidfieldGoalsAttack' => $totalMidfieldGoalsAttack,
            'avgGoalsAttack' => round($avgGoalsAttack, 2),
        ];

        $allDefendStats = [
            'totalGoalsDefend' => $totalGoalsDefend,
            'totalOwnGoalsDefend' => $totalOwnGoalsDefend,
            'avgGoalsDefend' => round($avgGoalsDefend, 2),
        ];

        if ($totalDaysGamesExisted > 0) {
            $avgGames = round($games->count() / $totalDaysGamesExisted, 2);
        } else {
            $avgGames = 0;
        }

        if ($totalDaysGamesExisted > 0) {
            $avgGoals = round($goals->count() / $totalDaysGamesExisted, 2);
        } else {
            $avgGoals = 0;
        }

        if ($totalDaysGamesExisted > 0) {
            $avgPlayTime = Carbon::parse($allGamesTotalGameTime / $totalDaysGamesExisted)->format("i:s");
        } else {
            $avgPlayTime = 0;
        }

        $allDayStats = [
            'avgGames' => $avgGames,
            'avgGoals' => $avgGoals,
            'avgPlayTime' => $avgPlayTime,
        ];

        return view('GeneralStatistics.generalStatistics')
            ->with('allGamesStats', $allGamesStats)
            ->with('allAttackStats', $allAttackStats)
            ->with('allDefendStats', $allDefendStats)
            ->with('allDayStats', $allDayStats);
    }
}
