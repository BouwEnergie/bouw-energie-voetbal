<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = User::all();

        return view('users.index')->with('users', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.createOrUpdate');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  UserRequest  $request
     * @return Response
     */
    public function store(UserRequest $request)
    {
        try {
            User::create($request->all());
        } catch (QueryException $e) {
            $request->session()->flash('error', 'Gebruiker aanmaken mislukt');
            return redirect()->back();
        } catch (\Exception $e) {
            $request->session()->flash('error', 'Gebruiker aanmaken mislukt');
            return redirect()->back();
        }
        echo "test";
        return redirect()->route('user.index')
            ->with('success', 'Gebruiker aangemaakt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(int $id)
    {
        $user = User::findOrFail($id);
        return view('users.show')
            ->with('user', $user);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $user
     * @return Response
     */
    public function edit(User $user)
    {
        return view('users.createOrUpdate')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserRequest  $request
     * @return Response
     */
    public function update(UserRequest $request)
    {
        try {
            $user = User::findOrFail($request->id);
            $user->update([
                'name' => $request->name,
                'email' => $request->email,
            ]);
        } catch (QueryException $e) {
            $request->session()->flash('error', 'Gebruiker bijwerken mislukt');
            return redirect()->back();
        } catch (\Exception $e) {
            $request->session()->flash('error', 'Gebruiker bijwerken mislukt');
            return redirect()->back();
        }
        $request->session()->flash('success', 'Gebruiker bijgewerkt');
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('user.index')
            ->with('success', 'Gebruiker verwijderd!');
    }
}
