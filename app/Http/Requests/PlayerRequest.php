<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PlayerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|string|max:250',
            'surname' => 'required|string|max:250',
            'date_of_birth' => 'nullable|date|before:today',
        ];
    }
    public function messages()
    {
        return [
            'firstname.required' => 'Voornaam is verplicht',
            'firstname.max' => 'Maximaal aantal karakters is 250',
            'surname.required' => 'Achternaam is verplicht',
            'surname.max' => 'Maximaal aantal karakters is 250',
            'date_of_birth.date' => 'Geen geldige datum',
            'date_of_birth.before' => 'Datum moet vóór de datum van vandaag zijn',
        ];
    }
}
