<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Contracts\Service\Attribute\Required;

class GameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id',
            'team_one_attack_player_one_id' => 'required',
            'team_one_defend_player_two_id' => 'required',
            'team_two_attack_player_three_id' => 'required',
            'team_two_defend_player_four_id' => 'required',
        ];
    }
}
