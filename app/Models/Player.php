<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Player extends Model
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    protected $appends = ['fullname'];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */

    protected $fillable = [
        'firstname',
        'surname',
        'date_of_birth',
    ];

    public function getFullnameAttribute()
    {
        return $this->firstname . ' ' . $this->surname;
    }

    public function goals()
    {
        return $this->hasMany(Goal::class, 'player_id');
    }

    public function teamOneAttackPlayerOneId()
    {
        return $this->hasMany(Game::class, 'team_one_attack_player_one_id', 'id');
    }

    public function teamOneDefendPlayerTwoId()
    {
        return $this->hasMany(Game::class, 'team_one_defend_player_two_id', 'id');
    }

    public function teamTwoAttackPlayerThreeId()
    {
        return $this->hasMany(Game::class, 'team_two_attack_player_three_id', 'id');
    }

    public function teamTwoDefendPlayerFourId()
    {
        return $this->hasMany(Game::class, 'team_two_defend_player_four_id', 'id');
    }

    public function countTotalGamesAttack()
    {
        $totalGamesPlayerOne = $this->teamOneAttackPlayerOneId()->count();
        $totalGamesPlayerThree = $this->teamTwoAttackPlayerThreeId()->count();
        $totalGamesAttack = collect([$totalGamesPlayerOne, $totalGamesPlayerThree])->sum();

        return $totalGamesAttack;
    }

    public function countTotalGamesDefend()
    {
        $totalGamesPlayerTwo = $this->teamOneDefendPlayerTwoId()->count();
        $totalGamesPlayerFour = $this->teamTwoDefendPlayerFourId()->count();
        $totalGamesDefend = collect([$totalGamesPlayerTwo, $totalGamesPlayerFour])->sum();

        return $totalGamesDefend;
    }

    public function countTotalGoalsAttack()
    {
        $player_id = $this->id;
        
        $totalGamesTeamOneAttack = $this->teamOneAttackPlayerOneId()->get();
        $totalGamesTeamTwoAttack = $this->teamTwoAttackPlayerThreeId()->get();

        $goalCountTeamOneAttack = 0;
        $goalCountTeamTwoAttack = 0;

        foreach ($totalGamesTeamOneAttack as $game) {
            $goalCountTeamOneAttack = $goalCountTeamOneAttack + Goal::where('game_id', $game->id)
            ->where('player_id', $player_id)
            ->where('own_goal', false)
            ->where('midfield_goal', false)
            ->count();
        }

        foreach ($totalGamesTeamTwoAttack as $game) {
            $goalCountTeamTwoAttack = $goalCountTeamTwoAttack + Goal::where('game_id', $game->id)
            ->where('player_id', $player_id)
            ->where('own_goal', false)
            ->where('midfield_goal', false)
            ->count();
        }
        $totalGoalsAttack = $goalCountTeamOneAttack + $goalCountTeamTwoAttack;
        return $totalGoalsAttack;
    }

    public function countTotalGoalsDefend()
    {
        $player_id = $this->id;
        
        $totalGamesTeamOneDefend = $this->teamOneDefendPlayerTwoId()->get();
        $totalGamesTeamTwoDefend = $this->teamTwoDefendPlayerFourId()->get();

        $goalCountTeamOneDefend = 0;
        $goalCountTeamTwoDefend = 0;

        foreach ($totalGamesTeamOneDefend as $game) {
            $goalCountTeamOneDefend = $goalCountTeamOneDefend + Goal::where('game_id', $game->id)
            ->where('player_id', $player_id)
            ->where('own_goal', false)
            ->where('midfield_goal', false)
            ->count();
        }

        foreach ($totalGamesTeamTwoDefend as $game) {
            $goalCountTeamTwoDefend = $goalCountTeamTwoDefend + Goal::where('game_id', $game->id)
            ->where('player_id', $player_id)
            ->where('own_goal', false)
            ->where('midfield_goal', false)
            ->count();
        }

        $totalGoalsDefend = $goalCountTeamOneDefend + $goalCountTeamTwoDefend;
        return $totalGoalsDefend;
    }

    public function countTotalOwnGoalsAttack()
    {
        $player_id = $this->id;
        $totalGamesTeamOneAttack = $this->teamOneAttackPlayerOneId()->get();
        $totalGamesTeamTwoAttack = $this->teamTwoAttackPlayerThreeId()->get();

        $ownGoalCountTeamOneAttack = 0;
        $ownGoalCountTeamTwoAttack = 0;

        foreach ($totalGamesTeamOneAttack as $game) {
            $ownGoalCountTeamOneAttack = $ownGoalCountTeamOneAttack + Goal::where('game_id', $game->id)
            ->where('player_id', $player_id)
            ->where('own_goal', true)->count();
        }

        foreach ($totalGamesTeamTwoAttack as $game) {
            $ownGoalCountTeamTwoAttack = $ownGoalCountTeamTwoAttack + Goal::where('game_id', $game->id)
            ->where('player_id', $player_id)
            ->where('own_goal', true)->count();
        }

        $totalOwnGoalsAttack = $ownGoalCountTeamOneAttack + $ownGoalCountTeamTwoAttack;
        return $totalOwnGoalsAttack;
    }

    public function countTotalMidfieldGoalsAttack()
    {
        $player_id = $this->id;
        $totalGamesTeamOneAttack = $this->teamOneAttackPlayerOneId()->get();
        $totalGamesTeamTwoAttack = $this->teamTwoAttackPlayerThreeId()->get();

        $midfieldGoalCountTeamOneAttack = 0;
        $midfieldGoalCountTeamTwoAttack = 0;

        foreach ($totalGamesTeamOneAttack as $game) {
            $midfieldGoalCountTeamOneAttack = $midfieldGoalCountTeamOneAttack + Goal::where('game_id', $game->id)
            ->where('player_id', $player_id)
            ->where('midfield_goal', true)->count();
        }

        foreach ($totalGamesTeamTwoAttack as $game) {
            $midfieldGoalCountTeamTwoAttack = $midfieldGoalCountTeamTwoAttack + Goal::where('game_id', $game->id)
            ->where('player_id', $player_id)
            ->where('midfield_goal', true)->count();
        }

        $totalMidfieldGoalsAttack = $midfieldGoalCountTeamOneAttack + $midfieldGoalCountTeamTwoAttack;
        return $totalMidfieldGoalsAttack;
    }

    public function countTotalOwnGoalsDefend()
    {
        $player_id = $this->id;
        $totalGamesTeamOneDefend = $this->teamOneDefendPlayerTwoId()->get();
        $totalGamesTeamTwoDefend = $this->teamTwoDefendPlayerFourId()->get();

        $ownGoalCountTeamOneDefend = 0;
        $ownGoalCountTeamTwoDefend = 0;

        foreach ($totalGamesTeamOneDefend as $game) {
            $ownGoalCountTeamOneDefend = $ownGoalCountTeamOneDefend + Goal::where('game_id', $game->id)
            ->where('player_id', $player_id)
            ->where('own_goal', true)->count();
        }

        foreach ($totalGamesTeamTwoDefend as $game) {
            $ownGoalCountTeamTwoDefend = $ownGoalCountTeamTwoDefend + Goal::where('game_id', $game->id)
            ->where('player_id', $player_id)
            ->where('own_goal', true)->count();
        }

        $totalOwnGoalsDefend = $ownGoalCountTeamOneDefend + $ownGoalCountTeamTwoDefend;
        return $totalOwnGoalsDefend;
    }

    public function winPercentageAttack()
    {
        $player_id = $this->id;
        $totalGamesTeamOneAttack = $this->teamOneAttackPlayerOneId()->get();
        $totalGamesTeamTwoAttack = $this->teamTwoAttackPlayerThreeId()->get();
        
        $gamesWonTeamOneAttack = 0;
        $gamesWonTeamTwoAttack = 0;
        $winPercentageAttack = 0;

        foreach ($totalGamesTeamOneAttack as $game) {
            $gamesWonTeamOneAttack = $gamesWonTeamOneAttack + Game::where('id', $game->id)
            ->where('team_one_attack_player_one_id', $player_id)
            ->where('team_one_total_score', 10)->count();
        }

        foreach ($totalGamesTeamTwoAttack as $game) {
            $gamesWonTeamTwoAttack = $gamesWonTeamTwoAttack + Game::where('id', $game->id)
            ->where('team_two_attack_player_three_id', $player_id)
            ->where('team_two_total_score', 10)->count();
        }

        $totalGamesWonAttack = $gamesWonTeamOneAttack + $gamesWonTeamTwoAttack;

        if ($gamesWonTeamOneAttack || $gamesWonTeamTwoAttack !== 0) {
            $winPercentageAttack = round($totalGamesWonAttack / ($totalGamesTeamOneAttack->count() + $totalGamesTeamTwoAttack
            ->count()) * 100, 2);
        } else {
            $winPercentageAttack = null;
        }
        
        
        return $winPercentageAttack;
    }

    public function winPercentageDefend()
    {
        $player_id = $this->id;
        $totalGamesTeamOneDefend = $this->teamOneDefendPlayerTwoId()->get();
        $totalGamesTeamTwoDefend = $this->teamTwoDefendPlayerFourId()->get();
        
        $gamesWonTeamOneDefend = 0;
        $gamesWonTeamTwoDefend = 0;
        $winPercentageDefend = 0;

        foreach ($totalGamesTeamOneDefend as $game) {
            $gamesWonTeamOneDefend = $gamesWonTeamOneDefend + Game::where('id', $game->id)
            ->where('team_one_defend_player_two_id', $player_id)
            ->where('team_one_total_score', 10)->count();
        }

        foreach ($totalGamesTeamTwoDefend as $game) {
            $gamesWonTeamTwoDefend = $gamesWonTeamTwoDefend + Game::where('id', $game->id)
            ->where('team_two_defend_player_four_id', $player_id)
            ->where('team_two_total_score', 10)->count();
        }

        $totalGamesWonDefend = $gamesWonTeamOneDefend + $gamesWonTeamTwoDefend;

        if ($totalGamesWonDefend > 0 || $totalGamesTeamOneDefend->count() > 0 || $totalGamesTeamTwoDefend
        ->count() > 0) {
            $winPercentageDefend = round($totalGamesWonDefend / ($totalGamesTeamOneDefend->count() + $totalGamesTeamTwoDefend
            ->count()) * 100, 2);
        } else {
            $winPercentageDefend = 0;
        }
        
        return $winPercentageDefend;
    }

    public function winPercentageTotal()
    {
        $player_id = $this->id;
        $totalGamesTeamOneAttack = $this->teamOneAttackPlayerOneId()->get();
        $totalGamesTeamTwoAttack = $this->teamTwoAttackPlayerThreeId()->get();
        $totalGamesTeamOneDefend = $this->teamOneDefendPlayerTwoId()->get();
        $totalGamesTeamTwoDefend = $this->teamTwoDefendPlayerFourId()->get();

        $totalGamesPlayed = $totalGamesTeamOneAttack->count() + $totalGamesTeamTwoAttack->count() + $totalGamesTeamOneDefend
        ->count() + $totalGamesTeamTwoDefend->count();
        
        $gamesWonTeamOneAttack = 0;
        $gamesWonTeamTwoAttack = 0;
        $gamesWonTeamOneDefend = 0;
        $gamesWonTeamTwoDefend = 0;
        $winPercentageTotal = 0;

        foreach ($totalGamesTeamOneAttack as $game) {
            $gamesWonTeamOneAttack = $gamesWonTeamOneAttack + Game::where('id', $game->id)
            ->where('team_one_attack_player_one_id', $player_id)
            ->where('team_one_total_score', 10)->count();
        }

        foreach ($totalGamesTeamTwoAttack as $game) {
            $gamesWonTeamTwoAttack = $gamesWonTeamTwoAttack + Game::where('id', $game->id)
            ->where('team_two_attack_player_three_id', $player_id)
            ->where('team_two_total_score', 10)->count();
        }

        foreach ($totalGamesTeamOneDefend as $game) {
            $gamesWonTeamOneDefend = $gamesWonTeamOneDefend + Game::where('id', $game->id)
            ->where('team_one_defend_player_two_id', $player_id)
            ->where('team_one_total_score', 10)->count();
        }

        foreach ($totalGamesTeamTwoDefend as $game) {
            $gamesWonTeamTwoDefend = $gamesWonTeamTwoDefend + Game::where('id', $game->id)
            ->where('team_two_defend_player_four_id', $player_id)
            ->where('team_two_total_score', 10)->count();
        }


        $totalGamesWon = $gamesWonTeamOneDefend + $gamesWonTeamTwoDefend + $gamesWonTeamOneAttack + $gamesWonTeamTwoAttack;

        if ($totalGamesWon > 0 || $totalGamesPlayed > 0) {
            $winPercentageTotal = round(($totalGamesWon / $totalGamesPlayed) * 100, 2);
        } else {
            $winPercentageTotal = 0;
        }
        
        return $winPercentageTotal;
    }

    public function countAllGames()
    {
        $allGames = Game::all()->count();

        return $allGames;
    }

    public function playerGameAppearanceAttack()
    {
        $allGames = $this->countAllGames();

        $totalGamesTeamOneAttack = $this->teamOneAttackPlayerOneId()->get();
        $totalGamesTeamTwoAttack = $this->teamTwoAttackPlayerThreeId()->get();
        $totalGamesPlayedAttack = $totalGamesTeamOneAttack->count() + $totalGamesTeamTwoAttack->count();

        $playerGameAppearanceAttack = round($totalGamesPlayedAttack / $allGames * 100, 2);

        return $playerGameAppearanceAttack;
    }

    public function playerGameAppearanceDefend()
    {
        $allGames = $this->countAllGames();

        $totalGamesTeamOneDefend = $this->teamOneDefendPlayerTwoId()->get();
        $totalGamesTeamTwoDefend = $this->teamTwoDefendPlayerFourId()->get();
        $totalGamesPlayedDefend = $totalGamesTeamOneDefend->count() + $totalGamesTeamTwoDefend->count();

        $playerGameAppearanceDefend = round($totalGamesPlayedDefend / $allGames * 100, 2);

        return $playerGameAppearanceDefend;
    }

    public function playerGameAppearanceTotal()
    {
        $allGames = $this->countAllGames();

        $totalGamesTeamOneAttack = $this->teamOneAttackPlayerOneId()->get();
        $totalGamesTeamTwoAttack = $this->teamTwoAttackPlayerThreeId()->get();
        $totalGamesTeamOneDefend = $this->teamOneDefendPlayerTwoId()->get();
        $totalGamesTeamTwoDefend = $this->teamTwoDefendPlayerFourId()->get();

        $totalGamesPlayed = $totalGamesTeamOneAttack->count() + $totalGamesTeamTwoAttack->count() + $totalGamesTeamOneDefend
        ->count() + $totalGamesTeamTwoDefend->count();

        $playerGameAppearanceTotal = round($totalGamesPlayed / $allGames * 100, 2);

        return $playerGameAppearanceTotal;
    }

    public function recentGame()
    {
        $playerRecentGame = [];
        $games = Game::all();

        foreach ($games as $game) {
            switch ($game) {
                case $game->team_one_attack_player_one_id == $this->id:
                    if ($game->team_one_attack_player_one_id == !null) {
                        $playerRecentGame[] = $game;
                    }
                    break;
                case $game->team_one_defend_player_two_id == $this->id:
                    if ($game->team_one_defend_player_two_id == !null) {
                        $playerRecentGame[] = $game;
                    }
                    break;
                case $game->team_two_attack_player_three_id == $this->id:
                    if ($game->team_two_attack_player_three_id == !null) {
                        $playerRecentGame[] = $game;
                    }
                    break;
                case $game->team_two_defend_player_four_id == $this->id:
                    if ($game->team_two_defend_player_four_id == !null) {
                        $playerRecentGame[] = $game;
                    }
                    break;
                default:
                        $playerRecentGame[] = null;
                    break;
            }
        }

        return last($playerRecentGame);
    }
}
