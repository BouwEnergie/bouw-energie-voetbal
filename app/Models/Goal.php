<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Goal extends Model
{
    use HasFactory, HasApiTokens, Notifiable;

    protected $fillable = [
        ''
    ];
    // Een goal heeft één game
    public function game()
    {
        return $this->hasOne(Game::class);
    }
    // Een goal heeft één player
    public function player()
    {
        return $this->hasOne(Player::class);
    }
}
