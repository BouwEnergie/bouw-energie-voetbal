<?php

namespace App\Models;

use App\Http\Controllers\GoalController;
use Facade\Ignition\DumpRecorder\Dump;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Laravel\Sanctum\HasApiTokens;

class Game extends Model
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;



    protected $fillable = [
        'user_id',
        'team_one_attack_player_one',
        'team_one_defend_player_two',
        'team_two_attack_player_third',
        'team_two_defend_player_four',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    // Een game wordt door één user aangemaakt
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    // Een game heeft een aanvallende player van team one
    public function teamOneAttackPlayerOneId()
    {
        return $this->hasOne(Player::class, 'id', 'team_one_attack_player_one_id');
    }
    // Een game heeft een verdedigende player van team one
    public function teamOneDefendPlayerTwoId()
    {
        return $this->hasOne(Player::class, 'id', 'team_one_defend_player_two_id');
    }
    // Een game heeft een aanvallende player van team two
    public function teamTwoAttackPlayerThreeId()
    {
        return $this->hasOne(Player::class, 'id', 'team_two_attack_player_three_id');
    }
    // Een game heeft een verdedigende player van team two
    public function teamTwoDefendPlayerFourId()
    {
        return $this->hasOne(Player::class, 'id', 'team_two_defend_player_four_id');
    }
    // Een game heeft meerdere goals
    public function goals()
    {
        return $this->hasMany(Goal::class, 'game_id');
    }
    
    public function calculateNewGoal(Goal $goal)
    {
        
        //team one
        if ($goal->player_id == $this->team_one_attack_player_one_id) {
            if ($goal->own_goal) {
                $this->team_two_total_score++;
            } elseif ($goal->midfield_goal) {
                if ($this->team_one_total_score > 0) {
                    $this->team_one_total_score--;
                }
            } else {
                $this->team_one_total_score++;
            }
        }

        if ($goal->player_id == $this->team_one_defend_player_two_id) {
            if ($goal->own_goal) {
                $this->team_two_total_score++;
            } else {
                $this->team_one_total_score++;
            }
        }

        //team two
        if ($goal->player_id == $this->team_two_attack_player_three_id) {
            if ($goal->own_goal) {
                $this->team_one_total_score++;
            } elseif ($goal->midfield_goal) {
                if ($this->team_two_total_score > 0) {
                    $this->team_two_total_score--;
                }
            } else {
                $this->team_two_total_score++;
            }
        }

        if ($goal->player_id == $this->team_two_defend_player_four_id) {
            if ($goal->own_goal) {
                $this->team_one_total_score++;
            } else {
                $this->team_two_total_score++;
            }
        }

        return $this;
    }

    public function players()
    {
        $players = [
            $this->team_one_attack_player_one_id,
            $this->team_one_defend_player_two_id,
            $this->team_two_attack_player_three_id,
            $this->team_two_defend_player_four_id,
        ];
        return $players;
    }

    public function getStatistics()
    {
        $goals = [];

        foreach ($this->players() as $player) {
            $goals[$player] = [
                'goal' => [],
                'ownGoal' => [],
                'midfieldGoal' => [],
            ];
        }

        foreach ($this->goals as $goal) {
            switch ($goal) {
                case $goal->midfield_goal == false && $goal->own_goal == false:
                    $goals[$goal->player_id]['goal'][] = $goal;
                    break;
                case $goal->own_goal == true:
                    $goals[$goal->player_id]['ownGoal'][] = $goal;
                    break;
                case $goal->midfield_goal == true:
                    $goals[$goal->player_id]['midfieldGoal'][] = $goal;
                    break;
            }
        }

        return $goals;
    }

    public function calculateGameDuration()
    {
        $gameCreatedTime = Carbon::parse($this->created_at);
        $gameUpdatedTime = Carbon::parse($this->updated_at);
        $totalGameTime = $gameCreatedTime->diff($gameUpdatedTime);
        return $totalGameTime;
    }

    public function dateOfGame()
    {
        $dateOfGame = Carbon::parse($this->created_at)->format("d-m-Y");

        return $dateOfGame;
    }

    public function generalGameStats()
    {
        $generalGameStats = [
            'totalGoals' => [],
            'totalOwnGoals' => [],
            'totalMidfieldGoals' => [],
            'averageTimePerGoal' => [],
        ];

        foreach ($this->goals as $goal) {
            switch ($goal) {
                case $goal->midfield_goal == false && $goal->own_goal == false:
                    $generalGameStats['totalGoals'][] = $goal;
                    break;
                case $goal->own_goal == true:
                    $generalGameStats['totalOwnGoals'][] = $goal;
                    break;
                case $goal->midfield_goal == true:
                    $generalGameStats['totalMidfieldGoals'][] = $goal;
                    break;
            }
        }

        return $generalGameStats;
    }

    public function statisticsTeamOne()
    {
        $statisticsTeamOne = [
            'ownGoals' => [],
            'midfieldGoals' => [],
            'avgTimePerGoal' => [],
            'winChance' => [],
        ];

        foreach ($this->goals as $goal) {
            if ($goal->player_id == $this->team_one_attack_player_one_id || $goal->player_id == $this->team_one_defend_player_two_id) {
                switch ($goal) {
                    case $goal->own_goal == true:
                        $statisticsTeamOne['ownGoals'][] = $goal;
                        break;
                }
            }
        }

        foreach ($this->goals as $goal) {
            if ($goal->player_id == $this->team_one_attack_player_one_id || $goal->player_id == $this->team_one_defend_player_two_id) {
                switch ($goal) {
                    case $goal->midfield_goal == true:
                        $statisticsTeamOne['midfieldGoals'][] = $goal;
                        break;
                }
            }
        }
        if ($this->teamOneAttackPlayerOneId()->first()->winPercentageTotal() == !null) {
            $winPercentagePlayerOneTeamOne = $this->teamOneAttackPlayerOneId()->first()->winPercentageTotal();
        } else {
            $winPercentagePlayerOneTeamOne = 0;
        }

        if ($this->teamOneAttackPlayerOneId()->first()->winPercentageTotal() == !null) {
            $winPercentagePlayerTwoTeamOne = $this->teamOneDefendPlayerTwoId()->first()->winPercentageTotal();
        } else {
            $winPercentagePlayerTwoTeamOne = 0;
        }

        if ($this->teamOneAttackPlayerOneId()->first()->winPercentageTotal() == !null) {
            $winPercentagePlayerThreeTeamTwo = $this->teamTwoAttackPlayerThreeId()->first()->winPercentageTotal();
        } else {
            $winPercentagePlayerOneTeamOne = 0;
        }

        if ($this->teamOneAttackPlayerOneId()->first()->winPercentageTotal() == !null) {
            $winPercentagePlayerFourTeamTwo = $this->teamTwoDefendPlayerFourId()->first()->winPercentageTotal();
        } else {
            $winPercentagePlayerFourTeamTwo = 0;
        }


        $winPercentagePlayerOneTeamOne = $this->teamOneAttackPlayerOneId()->first()->winPercentageTotal();
        $winPercentagePlayerTwoTeamOne = $this->teamOneDefendPlayerTwoId()->first()->winPercentageTotal();
        $winPercentagePlayerThreeTeamTwo = $this->teamTwoAttackPlayerThreeId()->first()->winPercentageTotal();
        $winPercentagePlayerFourTeamTwo = $this->teamTwoDefendPlayerFourId()->first()->winPercentageTotal();
        
        $winPercentageTeamOne = round(($winPercentagePlayerOneTeamOne + $winPercentagePlayerTwoTeamOne) / 2, 2);
        $winPercentageTeamTwo = round(($winPercentagePlayerThreeTeamTwo + $winPercentagePlayerFourTeamTwo) / 2, 2);

        $bothTeamsWinPercentage = round($winPercentageTeamOne + $winPercentageTeamTwo, 2);

        $winChanceTeamOne = round($winPercentageTeamOne / $bothTeamsWinPercentage * 100, 2);
        
        $statisticsTeamOne['winChance'][] = $winChanceTeamOne;

        return $statisticsTeamOne;
    }

    public function statisticsTeamTwo()
    {
        $statisticsTeamTwo = [
            'ownGoals' => [],
            'midfieldGoals' => [],
            'winChance' => [],
        ];

        foreach ($this->goals as $goal) {
            if ($goal->player_id == $this->team_two_attack_player_three_id || $goal->player_id == $this->team_two_defend_player_four_id) {
                switch ($goal) {
                    case $goal->own_goal == true:
                        $statisticsTeamTwo['ownGoals'][] = $goal;
                        break;
                }
            }
        }

        foreach ($this->goals as $goal) {
            if ($goal->player_id == $this->team_two_attack_player_three_id || $goal->player_id == $this->team_two_defend_player_four_id) {
                switch ($goal) {
                    case $goal->midfield_goal == true:
                        $statisticsTeamTwo['midfieldGoals'][] = $goal;
                        break;
                }
            }
        }
        $winPercentagePlayerOneTeamOne = $this->teamOneAttackPlayerOneId()->first()->winPercentageTotal();
        $winPercentagePlayerTwoTeamOne = $this->teamOneDefendPlayerTwoId()->first()->winPercentageTotal();
        $winPercentagePlayerThreeTeamTwo = $this->teamTwoAttackPlayerThreeId()->first()->winPercentageTotal();
        $winPercentagePlayerFourTeamTwo = $this->teamTwoDefendPlayerFourId()->first()->winPercentageTotal();

        $winPercentageTeamOne = round(($winPercentagePlayerOneTeamOne + $winPercentagePlayerTwoTeamOne) / 2, 2);
        $winPercentageTeamTwo = round(($winPercentagePlayerThreeTeamTwo + $winPercentagePlayerFourTeamTwo) / 2, 2);

        $bothTeamsWinPercentage = round($winPercentageTeamOne + $winPercentageTeamTwo, 2);

        $winChanceTeamTwo = round($winPercentageTeamTwo / $bothTeamsWinPercentage * 100, 2);

        $statisticsTeamTwo['winChance'][] = $winChanceTeamTwo;

        return $statisticsTeamTwo;
    }
}
